// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>
#include <stdint.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#define GL_GLEXT_PROTOTYPES 1
#include <GLES2/gl2ext.h>
#include <GLES3/gl31.h>
#include <wayland-client.h>
#include <wayland-egl.h>

#include "xdg-shell-client-protocol.h"

typedef signed char        s8;
typedef unsigned char      u8;
typedef signed short       s16;
typedef unsigned short     u16;
typedef signed int         s32;
typedef unsigned int       u32;
typedef float              f32;
typedef double             f64;
#if defined(__LP64__)
typedef long               s64;
typedef unsigned long u64;
#else
typedef long long          s64;
typedef unsigned long long u64;
#endif

#define STR(s)             #s
#define XSTR(s)            STR(s)
#define ARRAY_LENGTH(a)    (sizeof (a) / sizeof (a)[0])
#define ROUND_UP_N(x,pow2) (((x) + ((pow2) - 1)) & ~((pow2) - 1))
#define MIN(x,y)           (((x) < (y)) ? (x) : (y))
#define MAX(x,y)           (((x) > (y)) ? (x) : (y))
#define CLAMP(x,y,z)       MAX(y, MIN(x, z))

#define MM21_TILE_WIDTH     16
#define MM21_Y_TILE_HEIGHT  32
#define MM21_UV_TILE_HEIGHT 16
#define MM21_Y_TILE_SIZE    (MM21_TILE_WIDTH * MM21_Y_TILE_HEIGHT)
#define MM21_UV_TILE_SIZE   ((MM21_TILE_WIDTH / 2) * (MM21_UV_TILE_HEIGHT / 2) * 2)

typedef bool (*TaskFunc)(void *data);

struct Image {
	int width;
	int height;
	int stride;
	int h_stride;
	void *y;
	void *uv;
};

struct Geometry {
	int width;
	int height;
};

struct Surface {
	EGLSurface surface;
	EGLDisplay display;
	EGLContext context;
	EGLConfig config;
};

struct SurfaceWayland {
	struct Surface egl;

	struct wl_display *display;
	struct wl_registry *registry;
	struct wl_compositor *compositor;
	struct wl_surface *wl_surface;
	struct wl_seat *seat;
	struct wl_keyboard *keyboard;
	struct wl_pointer *pointer;
	struct wl_egl_window *native;
	struct wl_list output_list;         // struct Output::link
	struct wl_list window_output_list;  // struct WindowOutput::link
	struct xdg_surface *xdg_surface;
	struct xdg_toplevel *xdg_toplevel;
	struct xdg_wm_base *wm_base;
	const struct SurfaceWaylandHandlers *handlers;
	void *user_data;

	enum wl_output_transform buffer_transform;
	int buffer_scale;
	struct Geometry window_size;
	struct Geometry logical_size;
	struct Geometry buffer_size;
	bool update_geometry;
	bool wait_configure;
	bool fullscreen;
	bool maximized;
};

struct SurfaceWaylandHandlers {
	// Keyboard.
	void (*keyboard_key)(struct SurfaceWayland *surface,
			     uint32_t time,
			     uint32_t key,
			     uint32_t state);
	// Pointer.
	void (*pointer_enter)(struct SurfaceWayland *surface,
			      double x,
			      double y);
	void (*pointer_leave)(struct SurfaceWayland *surface);
	void (*pointer_motion)(struct SurfaceWayland *surface,
			       uint32_t time,
			       double x,
			       double y);
	void (*pointer_button)(struct SurfaceWayland *surface,
			       uint32_t time,
			       uint32_t button,
			       uint32_t state);
	void (*pointer_axis)(struct SurfaceWayland *surface,
			     uint32_t time,
			     uint32_t axis,
			     int value);
	// Window.
	void (*resize)(struct SurfaceWayland *surface,
		       const struct Geometry geometry);
	void (*close)(struct SurfaceWayland *surface);
};

struct FenceSyncFuncs {
	PFNEGLCREATESYNCKHRPROC create_sync;
	PFNEGLDESTROYSYNCKHRPROC destroy_sync;
	PFNEGLDUPNATIVEFENCEFDANDROIDPROC dup_native_fence_fd;
};

struct DisjointTimerQueryFuncs {
	PFNGLGENQUERIESEXTPROC gen_queries;
	PFNGLDELETEQUERIESEXTPROC delete_queries;
	PFNGLBEGINQUERYEXTPROC begin_query;
	PFNGLENDQUERYEXTPROC end_query;
	PFNGLGETQUERYOBJECTUI64VEXTPROC get_query_object_ui64v;
	PFNGLGETQUERYOBJECTIVEXTPROC get_query_object_iv;
};

void* load_file(const char *filename,
		int *size,
		bool add_null);

int load_image(struct Image *image,
	       const char *filename,
	       int width,
	       int height);

void* detile_image(const struct Image *image);

void destroy_image(struct Image *image);

bool check_egl_extension(const char *extensions,
			 const char *extension);

void *get_egl_proc_address(const char *address);

EGLDisplay get_egl_display(EGLenum platform,
			   void *native_display,
			   const EGLint *attributes);

struct Surface* create_surface(int gl_major,
			       int gl_minor);

void destroy_surface(struct Surface *surface);

struct SurfaceWayland* create_surface_wayland(int gl_major,
					      int gl_minor,
					      int width,
					      int height,
					      const char *title,
					      const char *id,
					      const struct SurfaceWaylandHandlers *handlers,
					      void *user_data);

void destroy_surface_wayland(struct SurfaceWayland *surface);

int dispatch_surface_wayland(struct SurfaceWayland *surface);

int compile_fragment_program(const char *vs_filename,
			     const char *fs_filename);

int compile_compute_program(const char *cs_filename);

bool init_fence_sync_funcs(struct FenceSyncFuncs *funcs,
			   EGLDisplay display);

bool init_disjoint_timer_query_funcs(struct DisjointTimerQueryFuncs *funcs);

u64 elapsed_time_cpu(int iterations,
		     TaskFunc task,
		     void *data);

u64 elapsed_time_gpu(EGLDisplay display,
		     int iterations,
		     TaskFunc task,
		     void *data);

#endif  // UTILS_H
