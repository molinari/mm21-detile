// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#ifndef COMMON_GLSL
#define COMMON_GLSL

// MM21 tiled 4:2:0 YCbCr format. 1 luminance plane, 1 colour difference plane
// with Cb/Cr components interleaved. Luminance and colour difference tiles have
// different heights.
#define MM21_TILE_WIDTH     16
#define MM21_Y_TILE_HEIGHT  32
#define MM21_UV_TILE_HEIGHT 16
#define MM21_Y_TILE_SIZE    (MM21_TILE_WIDTH * MM21_Y_TILE_HEIGHT)
#define MM21_UV_TILE_SIZE   ((MM21_TILE_WIDTH / 2) * (MM21_UV_TILE_HEIGHT / 2) * 2)

// FIXME Lower precision case by case and check perf.
precision highp int;
precision mediump float;

// Constructs to hint compilers at using special instructions.

// Most GPUs support free saturation on stores.
float SAT(float a) { return clamp(a,       0.0,       1.0); }
vec2  SAT(vec2  a) { return clamp(a, vec2(0.0), vec2(1.0)); }
vec3  SAT(vec3  a) { return clamp(a, vec3(0.0), vec3(1.0)); }
vec4  SAT(vec4  a) { return clamp(a, vec4(0.0), vec4(1.0)); }

// fma() is only exposed since GLSL ES 3.2, but most GPUs have fused MUL/ADD.
float MAD(float a, float b, float c) { return a * b + c; }
vec2  MAD(vec2  a, vec2  b, vec2  c) { return a * b + c; }
vec3  MAD(vec3  a, vec3  b, vec3  c) { return a * b + c; }
vec4  MAD(vec4  a, vec4  b, vec4  c) { return a * b + c; }

// Convert 4 packed pixels from YUV to YUVA.
uvec4
yuva_from_yuv_packed(uvec4 y, uvec4 u, uvec4 v)
{
	return uvec4(0xff000000u | v << uvec4(16u) | u << uvec4(8u) | y);
}

// Convert 4 packed pixels from YUV + alpha to YUVA.
uvec4
yuva_from_yuva_packed(uvec4 y, uvec4 u, uvec4 v, uvec4 a)
{
	vec4 yf = vec4(y) / 255.0;
	vec4 uf = vec4(u) / 255.0;
	vec4 vf = vec4(v) / 255.0;

	y = uvec4(yf * vec4(a));
	u = uvec4(uf * vec4(a));
	v = uvec4(vf * vec4(a));

	return uvec4(a << uvec4(24u) | v << uvec4(16u) | u << uvec4(8u) | y);
}

// Convert 4 packed pixels from YUV to YUVA.
uvec4
yuva_from_yuv(vec4 y, vec4 u, vec4 v)
{
	uvec4 yu = uvec4(y * 255.0);
	uvec4 uu = uvec4(u * 255.0);
	uvec4 vu = uvec4(v * 255.0);

	return uvec4(0xff000000u | vu << uvec4(16u) | uu << uvec4(8u) | yu);
}

// Convert 4 packed pixels from YUV + alpha to YUVA.
uvec4
yuva_from_yuva(vec4 y, vec4 u, vec4 v, vec4 a)
{
	uvec4 yu = uvec4(y * a * 255.0);
	uvec4 uu = uvec4(u * a * 255.0);
	uvec4 vu = uvec4(v * a * 255.0);
	uvec4 au = uvec4(a * 255.0);

	return uvec4(au << uvec4(24u) | vu << uvec4(16u) | uu << uvec4(8u) | yu);
}

// Convert 4 packed pixels from YUV to RGBA.
uvec4
rgba_from_yuv(vec4 y, vec4 u, vec4 v)
{
	y = MAD(y, vec4(1.16438356), vec4(-0.0727739725));
	u = u - 0.5;
	v = v - 0.5;

	vec4 r = SAT(MAD(v, vec4( 1.59602678), y));
	vec4 g =     MAD(u, vec4(-0.39176229), y);
	     g = SAT(MAD(v, vec4(-0.81296764), g));
	vec4 b = SAT(MAD(u, vec4( 2.01723214), y));

	uvec4 ru = uvec4(r * 255.0);
	uvec4 gu = uvec4(g * 255.0);
	uvec4 bu = uvec4(b * 255.0);

	return uvec4(0xff000000u | bu << uvec4(16u) | gu << uvec4(8u) | ru);
}

// Convert 4 packed pixels from YUV + alpha to RGBA.
uvec4
rgba_from_yuva(vec4 y, vec4 u, vec4 v, vec4 a)
{
	y = MAD(y, vec4(1.16438356), vec4(-0.0727739725));
	u = u - 0.5;
	v = v - 0.5;
	a *= 255.0;

	vec4 r = SAT(MAD(v, vec4( 1.59602678), y));
	vec4 g =     MAD(u, vec4(-0.39176229), y);
	     g = SAT(MAD(v, vec4(-0.81296764), g));
	vec4 b = SAT(MAD(u, vec4( 2.01723214), y));

	uvec4 ru = uvec4(r * a);
	uvec4 gu = uvec4(g * a);
	uvec4 bu = uvec4(b * a);

	return uvec4(uvec4(a) << uvec4(24u) | bu << uvec4(16u) | gu << uvec4(8u) | ru);
}

// Convert 4 packed pixels from YUV to RGBA.
uvec4
rgba_from_yuv_packed(uvec4 y, uvec4 u, uvec4 v)
{
	vec4 yf = MAD(vec4(y) / vec4(255.0), vec4(1.16438356), vec4(-0.0727739725));
	vec4 uf = (vec4(u) / 255.0) - 0.5;
	vec4 vf = (vec4(v) / 255.0) - 0.5;

	vec4 rf = SAT(MAD(vf, vec4( 1.59602678), yf));
	vec4 gf =     MAD(uf, vec4(-0.39176229), yf);
	     gf = SAT(MAD(vf, vec4(-0.81296764), gf));
	vec4 bf = SAT(MAD(uf, vec4( 2.01723214), yf));

	uvec4 ru = uvec4(rf * 255.0);
	uvec4 gu = uvec4(gf * 255.0);
	uvec4 bu = uvec4(bf * 255.0);

	return uvec4(0xff000000u | bu << uvec4(16u) | gu << uvec4(8u) | ru);
}

// Convert 4 packed pixels from YUV + alpha to RGBA.
uvec4
rgba_from_yuva_packed(uvec4 y, uvec4 u, uvec4 v, uvec4 a)
{
	vec4 yf = MAD(vec4(y) / vec4(255.0), vec4(1.16438356), vec4(-0.0727739725));
	vec4 uf = (vec4(u) / 255.0) - 0.5;
	vec4 vf = (vec4(v) / 255.0) - 0.5;

	vec4 rf = SAT(MAD(vf, vec4( 1.59602678), yf));
	vec4 gf =     MAD(uf, vec4(-0.39176229), yf);
	     gf = SAT(MAD(vf, vec4(-0.81296764), gf));
	vec4 bf = SAT(MAD(uf, vec4( 2.01723214), yf));

	uvec4 ru = uvec4(rf * vec4(a));
	uvec4 gu = uvec4(gf * vec4(a));
	uvec4 bu = uvec4(bf * vec4(a));

	return uvec4(a << uvec4(24u) | bu << uvec4(16u) | gu << uvec4(8u) | ru);
}

#endif  // COMMON_GLSL
