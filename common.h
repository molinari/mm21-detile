// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#ifndef COMMON_H
#define COMMON_H

#define INPUT_FILENAME_DEFAULT  "image.mm21"
#define ALPHA_FILENAME_DEFAULT  "alpha.mm21"
#define INPUT_WIDTH_DEFAULT     1920
#define INPUT_HEIGHT_DEFAULT    1080

#endif  // COMMON_H
