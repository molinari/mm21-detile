#version 310 es

// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include "common.glsl"

layout(location = 0) in vec2 v_tex_coord;

layout(location = 0) out vec4 color;

layout(binding = 0) uniform sampler2D tex;

void
main()
{
	color = texture(tex, v_tex_coord);
}
