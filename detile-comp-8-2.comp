#version 310 es

// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include "common.glsl"

// ARM GPU best practices developer guide recommends no more than 64 threads per
// workgroup and multiples of 4 workgroup sizes.

// 1 4x16 workgroup loads 2 entire Y tiles.
// 1 4x16 workgroup loads 4 entire UV tiles.
#define THREAD_X 4
#define THREAD_Y 16

layout(local_size_x = THREAD_X, local_size_y = THREAD_Y) in;

layout(location = 0) uniform uint tiles_per_stride;
layout(location = 1) uniform uint no_csc;

layout(rgba16ui, binding = 0) readonly  uniform highp uimage2D y_tiled;
layout(rgba16ui, binding = 1) readonly  uniform highp uimage2D uv_tiled;
layout(rgba32ui, binding = 2) writeonly uniform highp uimage2D rgba_linear;

// Convert from linear coordinates to tiled coordinates.
// Y coords are returned in x and z, UV coords are returned in y and w.
void
tiled_from_linear(in ivec2 linear, out ivec4 tiled1, out ivec4 tiled2)
{
	// Constants:
	//   kTileW    = MM21_TILE_WIDTH / 8               =  2
	//   kYTileH   = MM21_Y_TILE_HEIGHT                = 32
	//   kUVTileH  = MM21_UV_TILE_HEIGHT               = 16
	//   kYTileSz  = kTileW * kYTileH                  = 64
	//   kUVTileSz = (kTileW / 2) * (kUVTileH / 2) * 2 = 32

	// 2D tile coord addressed by 'linear' in the linear buffer (1st tile is
	// (0,0), 2nd tile is (1,0), etc):
	//   tlc = linear.xy / (kTileW, kYTileH)
	uvec2 tlc = uvec2(linear) >> uvec2(1u, 5u);

	// 2D texel coord addressed by 'linear' in the tile:
	//   txc = linear.xy % (kTileW, kYTileH)
	uvec2 txc = uvec2(linear) & uvec2(1u, 31u);

	// Tile offset.
	uint tlo = tlc.y * tiles_per_stride + tlc.x;

	// Texel offset in Y and UV buffers:
	//   txo.x = (txc.y * kTileW) + txc.x
	//   txo.y = (((linear.y / 2) % kUVTileH)      * kTileW) + txc.x
	//         = (((linear.y      % kYTileH ) / 2) * kTileW) + txc.x
	//         = ((txc.y                      / 2) * kTileW) + txc.x
	uvec4 txo = uvec4(txc.y << 1u, txc.y & ~1u,
			  (txc.y + 1u) << 1u, (txc.y + 1u) & ~1u) | txc.xxxx;

	// Global offset in Y and UV buffers:
	//   off.x = (tlo * kYTileSz) + txo.x
	//   off.y = (tlo * 2 * kUVTileSz) + txo.y
	uvec2 tloff = uvec2(tlo) << uvec2(6u, 5u);
	uvec4 off = tloff.xyxy | txo;

	// XXX Hard-coded for images of width of 1920 for now!
	//
	// Convert to 2D coord in the tiled buffer:
	//   tiled.xy = off % (1920 / 8, 1920 / 8)
	//   tiled.zw = off / (1920 / 8, 1920 / 8)
	//
	// Move div/mod computation to floating-point pipe using reciprocal mul
	// as it's very slow otherwise on the integer pipe.
	vec4 q = vec4(off) * vec4(0.00416666666666666667);
	tiled1 = ivec4(uvec2(fract(q.xy) * vec2(240.0)), uvec2(q.xy));
	tiled2 = ivec4(uvec2(fract(q.zw) * vec2(240.0)), uvec2(q.zw));
}

void
main()
{
	ivec2 linear;
	ivec4 tiled1, tiled2;
	uvec4 y1, y2, uv, rgba1, rgba2;
	uvec4 hi_y1, lo_y1, hi_y2, lo_y2, hi_uv, lo_uv, unpack_y1, unpack_y2;

	linear = ivec2(gl_GlobalInvocationID.x, gl_GlobalInvocationID.y << 1);
	tiled_from_linear(linear, tiled1, tiled2);

	y1 = imageLoad(y_tiled, tiled1.xz);
	y2 = imageLoad(y_tiled, tiled2.xz);
	uv = imageLoad(uv_tiled, tiled1.yw);

	hi_y1 = y1 >> 8u;
	hi_y2 = y2 >> 8u;
	hi_uv = uv >> 8u;
	lo_y1 = y1 & 255u;
	lo_y2 = y2 & 255u;
	lo_uv = uv & 255u;

	linear.x <<= 1;

	if (no_csc == uint(0)) {
		unpack_y1 = uvec4(lo_y1.x, hi_y1.x, lo_y1.y, hi_y1.y);
		unpack_y2 = uvec4(lo_y2.x, hi_y2.x, lo_y2.y, hi_y2.y);
		rgba1 = rgba_from_yuv_packed(unpack_y1, lo_uv.xxyy, hi_uv.xxyy);
		rgba2 = rgba_from_yuv_packed(unpack_y2, lo_uv.xxyy, hi_uv.xxyy);
		imageStore(rgba_linear, ivec2(linear.x, linear.y), rgba1);
		imageStore(rgba_linear, ivec2(linear.x, linear.y + 1), rgba2);

		unpack_y1 = uvec4(lo_y1.z, hi_y1.z, lo_y1.w, hi_y1.w);
		unpack_y2 = uvec4(lo_y2.z, hi_y2.z, lo_y2.w, hi_y2.w);
		rgba1 = rgba_from_yuv_packed(unpack_y1, lo_uv.zzww, hi_uv.zzww);
		rgba2 = rgba_from_yuv_packed(unpack_y2, lo_uv.zzww, hi_uv.zzww);
		imageStore(rgba_linear, ivec2(linear.x | 1, linear.y), rgba1);
		imageStore(rgba_linear, ivec2(linear.x | 1, linear.y + 1), rgba2);
	} else {
		unpack_y1 = uvec4(lo_y1.x, hi_y1.x, lo_y1.y, hi_y1.y);
		unpack_y2 = uvec4(lo_y2.x, hi_y2.x, lo_y2.y, hi_y2.y);
		rgba1 = yuva_from_yuv_packed(unpack_y1, lo_uv.xxyy, hi_uv.xxyy);
		rgba2 = yuva_from_yuv_packed(unpack_y2, lo_uv.xxyy, hi_uv.xxyy);
		imageStore(rgba_linear, ivec2(linear.x, linear.y), rgba1);
		imageStore(rgba_linear, ivec2(linear.x, linear.y + 1), rgba2);

		unpack_y1 = uvec4(lo_y1.z, hi_y1.z, lo_y1.w, hi_y1.w);
		unpack_y2 = uvec4(lo_y2.z, hi_y2.z, lo_y2.w, hi_y2.w);
		rgba1 = yuva_from_yuv_packed(unpack_y1, lo_uv.zzww, hi_uv.zzww);
		rgba2 = yuva_from_yuv_packed(unpack_y2, lo_uv.zzww, hi_uv.zzww);
		imageStore(rgba_linear, ivec2(linear.x | 1, linear.y), rgba1);
		imageStore(rgba_linear, ivec2(linear.x | 1, linear.y + 1), rgba2);
	}
}
