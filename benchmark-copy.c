// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include "benchmark.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <linux/input.h>

enum TextureId {
	TEXTURE_SRC = 0,
	TEXTURE_DST,
	TEXTURE_COUNT
};

enum ProgramId {
	PROGRAM_TEXTURE = 0,
	PROGRAM_COUNT
};

struct CopyData {
	struct SurfaceWayland *surface;
	unsigned int framebuffer;
	unsigned int programs[PROGRAM_COUNT];
	unsigned int textures[TEXTURE_COUNT];
	const struct CommandLine *cli;
	const struct Image *image;
	bool quit;
};

static void
handle_keyboard_key(struct SurfaceWayland *surface, uint32_t time, uint32_t key,
		    uint32_t state)
{
	struct CopyData *data = surface->user_data;

	switch (key) {
	case KEY_Q:
	case KEY_ESC:
		data->quit = true;
		break;
	default:
		break;
	}
}

static void
handle_close(struct SurfaceWayland *surface)
{
	struct CopyData *data = surface->user_data;

	data->quit = true;
}

static const struct SurfaceWaylandHandlers wayland_handlers = {
	.keyboard_key = handle_keyboard_key,
	.pointer_enter = NULL,
	.pointer_leave = NULL,
	.pointer_motion = NULL,
	.pointer_button = NULL,
	.pointer_axis = NULL,
	.resize = NULL,
	.close = handle_close
};

static struct VariantData*
create_copy(const struct CommandLine *cli, const struct Image *image,
	    const struct Image *alpha)
{
	int max_texture_size;
	u8 *linear_image;
	struct CopyData *copy;

	copy = calloc(1, sizeof *copy);
	assert(copy);
	copy->cli = cli;
	copy->image = image;

	if (cli->view)
		copy->surface = create_surface_wayland(
			3, 1, image->width, image->height, WINDOW_TITLE,
			WINDOW_ID, &wayland_handlers, copy);
	else
		copy->surface = (struct SurfaceWayland*) create_surface(3, 1);
	if (!copy->surface)
		goto error;

	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_texture_size);
	if (image->width > max_texture_size ||
	    image->height > max_texture_size) {
		fprintf(stderr, "Error: texture size %dx%d higher than "
			"implementation limit %d.\n", image->width,
			image->height, max_texture_size);
		goto error_surface;
	}

	copy->programs[PROGRAM_TEXTURE] = compile_fragment_program(
		TEXTURE_VS_FILENAME, TEXTURE_FS_FILENAME);
	if (!copy->programs[PROGRAM_TEXTURE])
		goto error_surface;

	glGenTextures(TEXTURE_COUNT, copy->textures);

	glGenFramebuffers(1, &copy->framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, copy->framebuffer);
	glBindTexture(GL_TEXTURE_2D, copy->textures[TEXTURE_DST]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, image->width, image->height);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			       GL_TEXTURE_2D, copy->textures[TEXTURE_DST], 0);

	linear_image = detile_image(image);
	assert(linear_image);
	glBindTexture(GL_TEXTURE_2D, copy->textures[TEXTURE_SRC]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, image->width, image->height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image->width, image->height,
			GL_RGBA, GL_UNSIGNED_BYTE, linear_image);
	free(linear_image);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glDisable(GL_BLEND);

	if (cli->view) {
		if (cli->fullscreen)
			xdg_toplevel_set_fullscreen(copy->surface->xdg_toplevel,
						    NULL);

		eglSwapInterval(copy->surface->egl.display, 0);
	}

	return (struct VariantData*) copy;

 error_surface:
	if (cli->view)
		destroy_surface_wayland(copy->surface);
	else
		destroy_surface((struct Surface*) copy->surface);
 error:
	free(copy);

	return NULL;
}

static void
destroy_copy(struct VariantData *data)
{
	struct CopyData *copy = (struct CopyData*) data;

	glDeleteFramebuffers(1, &copy->framebuffer);
	glDeleteTextures(TEXTURE_COUNT, copy->textures);
	glDeleteProgram(copy->programs[PROGRAM_TEXTURE]);
	if (copy->cli->view)
		destroy_surface_wayland(copy->surface);
	else
		destroy_surface((struct Surface*) copy->surface);
	free(copy);
}

static int
run_copy(struct VariantData *data)
{
	struct CopyData *copy = (struct CopyData*) data;

	static const struct { float x, y; } detile_positions[4] = {
		{ -1.0f, -1.0f },
		{  1.0f, -1.0f },
		{ -1.0f,  1.0f },
		{  1.0f,  1.0f }
	};
	static const struct { float s, t; } tex_coords[4] = {
		{ 0.0f, 0.0f },
		{ 1.0f, 0.0f },
		{ 0.0f, 1.0f },
		{ 1.0f, 1.0f }
	};

	// Copy.
	glBindFramebuffer(GL_FRAMEBUFFER, copy->framebuffer);
	glViewport(0, 0, copy->image->width, copy->image->height);
	glBindTexture(GL_TEXTURE_2D, copy->textures[TEXTURE_SRC]);
	glUseProgram(copy->programs[PROGRAM_TEXTURE]);
	glVertexAttribPointer(0, 2, GL_FLOAT, 0, 0, detile_positions);
	glVertexAttribPointer(1, 2, GL_FLOAT, 0, 0, tex_coords);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	if (copy->cli->view) {
		struct SurfaceWayland *surface = copy->surface;
		float a = surface->buffer_size.width * copy->image->height;
		float b = surface->buffer_size.height * copy->image->width;
		float ratio_x = MIN(1.0f, b / a);
		float ratio_y = MIN(1.0f, a / b);
		struct { float x, y; } view_positions[4] = {
			{ -ratio_x,  ratio_y },
			{  ratio_x,  ratio_y },
			{ -ratio_x, -ratio_y },
			{  ratio_x, -ratio_y }
		};
		struct wl_region *region;

		// Draw.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, surface->buffer_size.width,
			   surface->buffer_size.height);
		glClear(GL_COLOR_BUFFER_BIT);
		glBindTexture(GL_TEXTURE_2D, copy->textures[TEXTURE_DST]);
		glUseProgram(copy->programs[PROGRAM_TEXTURE]);
		glVertexAttribPointer(0, 2, GL_FLOAT, 0, 0, view_positions);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		// Present.
		region = wl_compositor_create_region(surface->compositor);
		wl_region_add(region, 0, 0, INT32_MAX, INT32_MAX);
		wl_surface_set_opaque_region(surface->wl_surface, region);
		wl_region_destroy(region);
		eglSwapBuffers(surface->egl.display, surface->egl.surface);

		return dispatch_surface_wayland(copy->surface) || copy->quit ||
			sigint;
	} else {
		// Flush and sync with GPU to get decent timings.
		if (copy->cli->timing == TIMING_CPU)
			glFinish();

		return sigint;
	}
}

static u8*
export_copy(struct VariantData *data)
{
	struct CopyData *copy = (struct CopyData*) data;
	u8 *pixels;

	pixels = malloc(copy->image->stride * copy->image->height * 4);
	assert(pixels);

	glReadPixels(0, 0, copy->image->stride, copy->image->height, GL_RGBA,
		     GL_UNSIGNED_BYTE, pixels);

	return pixels;
}

const struct VariantFuncs copy_funcs = {
	.create = create_copy,
	.destroy = destroy_copy,
	.run = run_copy,
	.export = export_copy
};
