#version 310 es

// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 offsets;

layout(location = 0) out vec3 v_offsets;

void main()
{
	gl_Position = position;
	v_offsets = offsets;
}
