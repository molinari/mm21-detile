#version 310 es

// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include "common.glsl"

#define DEBUG_Y_COORD  0
#define DEBUG_UV_COORD 1
#define DEBUG_Y_PLANE  2
#define DEBUG_U_PLANE  3
#define DEBUG_V_PLANE  4

layout(location = 0) in vec2 v_tex_coord;

layout(location = 0) out vec4 color;

layout(location = 0) uniform vec2 viewport_size;
layout(location = 1) uniform vec2 image_size;
layout(location = 2) uniform ivec2 y_tex_size;
layout(location = 3) uniform ivec2 uv_tex_size;
layout(location = 4) uniform int bilinear;
layout(location = 5) uniform int debug_mode;

layout(binding = 0) uniform sampler2D y_tex;
layout(binding = 1) uniform sampler2D uv_tex;

void
resolve(ivec2 coord, int y_stride, out ivec2 y_coord, out ivec2 uv_coord)
{
	int tiles_per_stride = y_stride / MM21_TILE_WIDTH;

	ivec2 tile_coord = coord / ivec2(MM21_TILE_WIDTH, MM21_Y_TILE_HEIGHT);
	ivec2 texel_coord = coord % ivec2(MM21_TILE_WIDTH, MM21_Y_TILE_HEIGHT);
	int tile_offset = tile_coord.t * tiles_per_stride + tile_coord.s;
	int texel_offset = texel_coord.t * MM21_TILE_WIDTH + texel_coord.s;
	int offset = tile_offset * MM21_Y_TILE_SIZE + texel_offset;
	y_coord = ivec2(offset % y_stride,
			offset / y_stride);

	texel_coord.t = (coord.t / 2) % MM21_UV_TILE_HEIGHT;
	texel_offset = texel_coord.t * MM21_TILE_WIDTH + texel_coord.s;
	offset = tile_offset * 2 * MM21_UV_TILE_SIZE + texel_offset;
	uv_coord = ivec2((offset / 2) % (y_stride / 2),
			 (offset / 2) / (y_stride / 2));
}

vec3
convert(vec3 yuv)
{
	float y = 1.16438356 * (yuv.x - 0.0625);
	float u = yuv.y - 0.5;
	float v = yuv.z - 0.5;

	return clamp(vec3(y + (1.59602678 * v),
			  y - (0.39176229 * u) - (0.81296764 * v),
			  y + (2.01723214 * u)),
		     vec3(0.0), vec3(1.0));
}

vec4
debug_y_coord(sampler2D y_tex, sampler2D uv_tex, vec2 tex_coord,
	      vec2 image_size, ivec2 y_tex_size)
{
	ivec2 icoord, y_coord, uv_coord;

	icoord = ivec2(tex_coord * image_size);
	resolve(icoord, y_tex_size.x, y_coord, uv_coord);

	return vec4((vec2(y_coord) / vec2(y_tex_size)).ts, 0.0, 1.0);
}

vec4
debug_uv_coord(sampler2D y_tex, sampler2D uv_tex, vec2 tex_coord,
	       vec2 image_size, ivec2 y_tex_size, ivec2 uv_tex_size)
{
	ivec2 icoord, y_coord, uv_coord;

	icoord = ivec2(tex_coord * image_size);
	resolve(icoord, y_tex_size.x, y_coord, uv_coord);

	return vec4((vec2(uv_coord) / vec2(uv_tex_size)).ts, 0.0, 1.0);
}

vec4
debug_y_plane(sampler2D y_tex, sampler2D uv_tex, vec2 tex_coord,
	      vec2 image_size, ivec2 y_tex_size)
{
	ivec2 icoord, y_coord, uv_coord;

	icoord = ivec2(tex_coord * image_size);
	resolve(icoord, y_tex_size.x, y_coord, uv_coord);

	return vec4(texelFetch(y_tex, y_coord, 0).rrr, 1.0);
}

vec4
debug_u_plane(sampler2D y_tex, sampler2D uv_tex, vec2 tex_coord,
	      vec2 image_size, ivec2 y_tex_size)
{
	ivec2 icoord, y_coord, uv_coord;

	icoord = ivec2(tex_coord * image_size);
	resolve(icoord, y_tex_size.x, y_coord, uv_coord);

	return vec4(texelFetch(uv_tex, uv_coord, 0).rrr, 1.0);
}

vec4
debug_v_plane(sampler2D y_tex, sampler2D uv_tex, vec2 tex_coord,
	      vec2 image_size, ivec2 y_tex_size)
{
	ivec2 icoord, y_coord, uv_coord;

	icoord = ivec2(tex_coord * image_size);
	resolve(icoord, y_tex_size.x, y_coord, uv_coord);

	return vec4(texelFetch(uv_tex, uv_coord, 0).ggg, 1.0);
}

vec4
nearest_lookup(sampler2D y_tex, sampler2D uv_tex, vec2 tex_coord,
	       vec2 image_size, int y_stride)
{
	ivec2 icoord, y_coord, uv_coord;

	icoord = ivec2(tex_coord * image_size);
	resolve(icoord, y_stride, y_coord, uv_coord);

	return vec4(convert(vec3(texelFetch(y_tex,  y_coord,  0).r,
				 texelFetch(uv_tex, uv_coord, 0).rg)), 1.0);
}

vec4
bilinear_lookup(sampler2D y_tex, sampler2D uv_tex, vec2 tex_coord,
		vec2 image_size, int y_stride)
{
	vec2 coord, factor;
	ivec2 icoord, y_coord, uv_coord;
	vec3 rgb00, rgb10, rgb01, rgb11;

	coord = tex_coord * image_size - vec2(0.5);
	factor = fract(coord);
	icoord = ivec2(coord);

	resolve(icoord + ivec2(0, 0), y_stride, y_coord, uv_coord);
	rgb00 = convert(vec3(texelFetch(y_tex,  y_coord,  0).r,
			     texelFetch(uv_tex, uv_coord, 0).rg));

	resolve(icoord + ivec2(1, 0), y_stride, y_coord, uv_coord);
	rgb10 = convert(vec3(texelFetch(y_tex,  y_coord,  0).r,
			     texelFetch(uv_tex, uv_coord, 0).rg));

	resolve(icoord + ivec2(0, 1), y_stride, y_coord, uv_coord);
	rgb01 = convert(vec3(texelFetch(y_tex,  y_coord, 0).r,
			     texelFetch(uv_tex, uv_coord, 0).rg));

	resolve(icoord + ivec2(1, 1), y_stride, y_coord, uv_coord);
	rgb11 = convert(vec3(texelFetch(y_tex,  y_coord, 0).r,
			     texelFetch(uv_tex, uv_coord, 0).rg));

	return vec4(mix(mix(rgb00, rgb10, factor.x),
			mix(rgb01, rgb11, factor.x), factor.y), 1.0);
}

vec4
checkerboard(vec2 frag_coord, float viewport_height)
{
	vec2 frag = vec2(frag_coord.x, viewport_height - frag_coord.y);
	ivec2 coord = ivec2(floor(frag * vec2(0.07)));
	float pattern = float((coord.x + coord.y) % 2);

	return vec4(vec3(pattern * 0.2 + 0.2), 1.0);
}

void
main()
{
	vec4 image, background;

	if (v_tex_coord.s < 0.0 || v_tex_coord.s >= 1.0 ||
	    v_tex_coord.t < 0.0 || v_tex_coord.t >= 1.0)
		image = vec4(0.0);

	else if (debug_mode == DEBUG_Y_COORD)
		image = debug_y_coord(y_tex, uv_tex, v_tex_coord, image_size,
				      y_tex_size);
	else if (debug_mode == DEBUG_UV_COORD)
		image = debug_uv_coord(y_tex, uv_tex, v_tex_coord, image_size,
				       y_tex_size, uv_tex_size);

	else if (debug_mode == DEBUG_Y_PLANE)
		image = debug_y_plane(y_tex, uv_tex, v_tex_coord, image_size,
				      y_tex_size);
	else if (debug_mode == DEBUG_U_PLANE)
		image = debug_u_plane(y_tex, uv_tex, v_tex_coord, image_size,
				      y_tex_size);
	else if (debug_mode == DEBUG_V_PLANE)
		image = debug_v_plane(y_tex, uv_tex, v_tex_coord, image_size,
				      y_tex_size);

	else if (bilinear != 0)
		image = bilinear_lookup(y_tex, uv_tex, v_tex_coord, image_size,
					y_tex_size.x);
	else
		image = nearest_lookup(y_tex, uv_tex, v_tex_coord, image_size,
				       y_tex_size.x);

	background = checkerboard(gl_FragCoord.xy, viewport_size.y);
	color = image + vec4(1.0 - image.a) * background;
}
