#!/usr/bin/env python3

# SPDX-FileCopyrightText:  © 2024 Collabora Ltd
# SPDX-License-Identifier: MIT

import os
import sys

from dataclasses import dataclass
from enum import Enum, auto
from struct import unpack


# MM21 tiled 4:2:0 Y'CbCr format. 1 luma plane, 1 colour difference (chroma)
# plane with Cb/Cr components interleaved. Luma and chroma tiles have different
# heights.
MM21_TILE_WIDTH     = 16
MM21_Y_TILE_HEIGHT  = 32
MM21_UV_TILE_HEIGHT = 16
MM21_Y_TILE_SIZE    = MM21_TILE_WIDTH * MM21_Y_TILE_HEIGHT
MM21_UV_TILE_SIZE   = (MM21_TILE_WIDTH // 2) * (MM21_UV_TILE_HEIGHT // 2) * 2

# Recommendation ITU-R BT.601-7, section 2.5.1.
BT_601_7_KR = 0.2990
BT_601_7_KB = 0.1140

# Recommendation ITU-R BT.709-6, section 3.2.
BT_709_6_KR = 0.2126
BT_709_6_KB = 0.0722

class Encoding(Enum):
    BT_601_7 = auto()
    BT_709_6 = auto()

@dataclass
class Coord:
    x: int = 0
    y: int = 0


def tiled_from_linear_ref(coord, stride):
    tiles_per_stride = stride // MM21_TILE_WIDTH

    # tlc = Coord(coord.x // MM21_TILE_WIDTH, coord.y // MM21_Y_TILE_HEIGHT)
    tlc = Coord(coord.x >> 4, coord.y >> 5)
    # txc = Coord(coord.x % MM21_TILE_WIDTH, coord.y % MM21_Y_TILE_HEIGHT)
    txc = Coord(coord.x & 15, coord.y & 31)

    tlo = tlc.y * tiles_per_stride + tlc.x
    txo = txc.y * MM21_TILE_WIDTH + txc.x

    yo = tlo * MM21_Y_TILE_SIZE + txo;

    # txc.y = (coord.y // 2) % MM21_UV_TILE_HEIGHT
    txc.y = (coord.y >> 1) & 15
    txo = txc.y * MM21_TILE_WIDTH + txc.x

    co = tlo * 2 * MM21_UV_TILE_SIZE + txo

    return yo, co


def rgb_from_ycbcr(y, cb, cr, encoding = Encoding.BT_709_6):
    # Luma is in [16, 235] and chroma in [16, 240] but out-of-range values are
    # allowed and must be handled appropriately (see saturation step below).
    assert 0 <= y <= 255 and 0 <= cb <= 255 and 0 <= cr <= 255

    # Basic implementation inverting ITU-R recommendations for reference.

    # Normalised coefficients.
    if encoding == Encoding.BT_601_7:
        kr = BT_601_7_KR
        kb = BT_601_7_KB
    elif encoding == Encoding.BT_709_6:
        kr = BT_709_6_KR
        kb = BT_709_6_KB
    assert 0 < kr + kb < 1
    kg = 1 - kr - kb

    # De-quantize 8-bit luma from [16, 235] and 8-bit chroma from [16, 240].
    bias = 16
    peak_y = 235
    peak_cbcr = 240
    ecr = (cr - 128) * (255 / (peak_cbcr - bias))
    ecb = (cb - 128) * (255 / (peak_cbcr - bias))
    ey = (y - bias) * (255 / (peak_y - bias))

    # R'G'B' inverse transforms.
    r = int(((ecr * (2 * (1 - kr))) + ey) + 0.5)
    b = int(((ecb * (2 * (1 - kb))) + ey) + 0.5)
    g = int(((ey - kr * r - kb * b) / kg) + 0.5)

    # Saturate to [0, 255] for out-of-range values.
    r = min(max(r, 0), 255)
    g = min(max(g, 0), 255)
    b = min(max(b, 0), 255)

    return 0xff000000 | b << 16 | g << 8 | r;

    #return r, g, b


def usage():
    print(f'MM21 Compare\n'
          f'    Compare RGBA to reference MM21 image.\n\n'
          f'Usage:\n'
          f'    compare.py [<options>]\n\n'
          f'Options:\n'
          f'    -h, --help\n'
          f'        Show this help.\n\n'
          f'    -w, --width <n>\n'
          f'        Width of MM21 and RGBA images.\n\n'
          f'    -H, --height <n>\n'
          f'        Height of MM21 and RGBA images.\n\n'
          f'    -m, --mm21 <str>\n'
          f'        MM21 reference image.\n\n'
          f'    -r, --rgba <str>\n'
          f'        RGBA image.\n')


def from_str(string, func, default = 0):
    try:
        return func(string)
    except:
        return default


def parse_cli(args):
    width = 0
    height = 0
    mm21 = None
    rgba = None
    args_iterator: Iterator[int] = iter(range(1, len(args)))

    for i in args_iterator:
        arg = args[i]
        skip_next = False
        if arg in ('-h', '--help'):
            usage()
            sys.exit(0)
        elif arg in ('-w', '--width'):
            width = i + 1 < len(args) and from_str(args[i + 1], int, width)
            skip_next = True
        elif arg in ('-H', '--height'):
            height = i + 1 < len(args) and from_str(args[i + 1], int, height)
            skip_next = True
        elif arg in ('-m', '--mm21'):
            mm21 = i + 1 < len(args) and args[i + 1] or mm21
            skip_next = True
        elif arg in ('-r', '--rgba'):
            rgba = i + 1 < len(args) and args[i + 1] or rgba
            skip_next = True
        skip_next and i + 1 < len(args) and next(args_iterator)

    if width < 1 or height < 1:
        print(f'Error: Invalid size {width}x{height}.')
        sys.exit(1)
    if mm21 == None:
        print(f'Error: MM21 reference image not set.')
        sys.exit(1)
    if rgba == None:
        print(f'Error: RGBA image not set.')
        sys.exit(1)

    return width, height, mm21, rgba


def open_files(mm21_name, rgba_name):
    try:
        mm21_file = open(mm21_name, 'rb')
    except OSError:
        print(f'Error: Cannot open MM21 file \'{mm21_name}\'.')
        return 1

    try:
        rgba_file = open(rgba_name, 'rb')
    except OSError:
        print(f'Error: Cannot open RGBA file \'{rgba_name}\'.')
        return 1

    return (mm21_file, rgba_file)


# Round up to the next power of 2.
def round_up_pow2(a, pow2):
    assert a > 0
    assert pow2 > 0 and (pow2 - 1) & pow2 == 0  # Is power of 2?
    return (a + (pow2 - 1)) & ~(pow2 - 1)


def compare(width, height, mm21_file, rgba_file):
    # MM21 frame layout.
    w_stride = round_up_pow2(width, MM21_TILE_WIDTH);
    h_stride = round_up_pow2(height, MM21_Y_TILE_HEIGHT);
    nw = w_stride // MM21_TILE_WIDTH;
    nh = h_stride // MM21_Y_TILE_HEIGHT;
    y_size = nw * nh * MM21_Y_TILE_SIZE;
    nh = h_stride // MM21_UV_TILE_HEIGHT;
    cbcr_size = nw * nh * MM21_UV_TILE_SIZE;
    mm21_size = y_size + cbcr_size;

    # Open MM21 reference image.
    mm21 = mm21_file.read()
    file_size = len(mm21)
    if file_size != mm21_size:
        print(f'Error: Invalid MM21 reference image, must be {mm21_size} bytes '
              f'not {file_size}.')
        return 1

    # Open RGBA image.
    rgba = rgba_file.read()
    file_size = len(rgba)
    rgba_size = width * height * 4
    if file_size != rgba_size:
        print(f'Error: Invalid RGBA image, must be {rgba_size} bytes not '
              f'{file_size}.')
        return 1
    rgba = unpack('I' * (rgba_size // 4), rgba)

    for j in range(height):
        for i in range(width):
            c = tiled_from_linear_ref(Coord(i, j), w_stride)
            y, cb, cr = mm21[c[0]], mm21[y_size + c[1]], mm21[y_size + c[1] + 1]
            r = rgb_from_ycbcr(y, cb, cr, Encoding.BT_601_7)
            p = rgba[j * width + i]
            rr, rg, rb = r & 0xff, (r >> 8) & 0xff, (r >> 16) & 0xff
            pr, pg, pb = p & 0xff, (p >> 8) & 0xff, (p >> 16) & 0xff
            if abs(rr - pr) >= 2 or abs(rg - pg) >= 2 or abs(rb - pb) >= 2:
                print(f'Error: Index:{i},{j} | YCbCr:{y},{cb},{cr} | '
                      f'Ref:{r:08X} | Cmp:{p:08X}')
                return 1


def main(args):
    width, height, mm21_name, rgba_name = parse_cli(args)

    files = open_files(mm21_name, rgba_name)
    if files == 1:
        return 1

    if compare(width, height, files[0], files[1]):
        return 1

    files[0].close()
    files[1].close()

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))





# def main(args):
#     if len(args) != 3:
#         print(f'Usage: {args[0]} <ref-image> <test-image>')
#         return 1

#     img1 = Image.open(args[1])
#     img2 = Image.open(args[2])

#     if img1.width != img2.width or img1.height != img2.height:
#         print(f'FAIL\tSize {img2.width}x{img2.height} don\'t match ref '
#               f'{img2.width}x{img2.height}.')
#         return 1

#     pix1 = img1.load()
#     pix2 = img2.load()

#     # FIXME Tests don't pass for now most likely because of csp conversions. But
#     # even with a fuzzy test, some pixels are different. Off-by-one?
#     for i in range(img1.height):
#         for j in range(img1.width):
#             diff = [abs(x - y) > 5 for x, y in zip(pix1[j, i], pix2[j, i])]
#             if diff != [False] * 4:
#                 print(f'FAIL\tPixel {pix2[j, i]} @ ({j}, {i}) don\'t '
#                       f'match ref {pix1[j, i]}.')
#                 return 1

#     return 0

# if __name__ == '__main__':
#     sys.exit(main(sys.argv))
