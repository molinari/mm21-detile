// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include "benchmark.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

enum TextureId {
	TEXTURE_Y = 0,
	TEXTURE_UV,
	TEXTURE_RGBA,
	TEXTURE_ALPHA,
	TEXTURE_COUNT
};

enum ProgramId {
	PROGRAM_DETILE = 0,
	PROGRAM_COUNT
};

struct CompData {
	struct Surface *surface;
	unsigned int framebuffer;
	unsigned int programs[PROGRAM_COUNT];
	unsigned int textures[TEXTURE_COUNT];
	const struct CommandLine *cli;
	const struct Image *image;
};

static const struct PixelsPerThreadInfo {
	const char *filename;
	const char *alpha_filename;
	GLenum format, type;
	int count;
	int x_threads, y_threads;
} ppt_info[] = {
	{ DETILE_COMP_4_FS_FILENAME,
	  DETILE_ALPHA_COMP_4_FS_FILENAME,
	  GL_RGBA8UI,  GL_UNSIGNED_BYTE,   4, 4, 16 },
	{ DETILE_COMP_8_FS_FILENAME,
	  DETILE_ALPHA_COMP_8_FS_FILENAME,
	  GL_RGBA16UI, GL_UNSIGNED_SHORT,  8, 2, 32 },
	{ DETILE_COMP_8_2_FS_FILENAME,
	  DETILE_ALPHA_COMP_8_2_FS_FILENAME,
	  GL_RGBA16UI, GL_UNSIGNED_SHORT,  8, 4, 16 },
	{ DETILE_COMP_16_FS_FILENAME,
	  DETILE_ALPHA_COMP_16_FS_FILENAME,
	  GL_RGBA32UI, GL_UNSIGNED_INT,   16, 2, 32 },
	{ DETILE_COMP_16_2_FS_FILENAME,
	  DETILE_ALPHA_COMP_16_2_FS_FILENAME,
	  GL_RGBA32UI, GL_UNSIGNED_INT,   16, 4, 16 }
};

static struct VariantData*
create_comp(const struct CommandLine *cli, const struct Image *image,
	    const struct Image *alpha)
{
	const struct PixelsPerThreadInfo *info =
		&ppt_info[cli->variant - VARIANT_COMP_4];
	int y_tex_width, y_tex_height, uv_tex_width, uv_tex_height;
	int max_texture_size;
	unsigned int program;
	struct CompData *comp;

	comp = calloc(1, sizeof *comp);
	assert(comp);
	comp->cli = cli;
	comp->image = image;

	// View is not supported because the destination texture texel size is
	// 128-bit (to reduce stores) and it can't be sampled as 32-bit with a
	// view shader.
	if (cli->view) {
		fprintf(stderr, "Compute shader variants do not support the "
			"view option.\n");
		goto error;
	}

	comp->surface = create_surface(3, 1);
	if (!comp->surface)
		goto error;

	y_tex_width = image->stride / info->count;
	y_tex_height = image->h_stride;
	uv_tex_width = y_tex_width;
	uv_tex_height = y_tex_height / 2;

	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_texture_size);
	if (y_tex_width > max_texture_size || y_tex_height > max_texture_size) {
		fprintf(stderr, "Error: Y texture size %dx%d higher than "
			"implementation limit %d.\n", y_tex_width, y_tex_height,
			max_texture_size);
		goto error_surface;
	}

	comp->programs[PROGRAM_DETILE] = compile_compute_program(
		 alpha ? info->alpha_filename : info->filename);
	if (!comp->programs[PROGRAM_DETILE])
		goto error_surface;

	glGenTextures(TEXTURE_COUNT, comp->textures);
	glGenFramebuffers(1, &comp->framebuffer);

	glBindTexture(GL_TEXTURE_2D, comp->textures[TEXTURE_Y]);
	glTexStorage2D(GL_TEXTURE_2D, 1, info->format, y_tex_width,
		       y_tex_height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, y_tex_width, y_tex_height,
			GL_RGBA_INTEGER, info->type, image->y);
	glBindImageTexture(0, comp->textures[TEXTURE_Y], 0, GL_FALSE, 0,
			   GL_READ_ONLY, info->format);

	glBindTexture(GL_TEXTURE_2D, comp->textures[TEXTURE_UV]);
	glTexStorage2D(GL_TEXTURE_2D, 1, info->format, uv_tex_width,
		       uv_tex_height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, uv_tex_width, uv_tex_height,
			GL_RGBA_INTEGER, info->type, image->uv);
	glBindImageTexture(1, comp->textures[TEXTURE_UV], 0, GL_FALSE, 0,
			   GL_READ_ONLY, info->format);

	glBindTexture(GL_TEXTURE_2D, comp->textures[TEXTURE_RGBA]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32UI, image->stride / 4,
		       y_tex_height);
	glBindImageTexture(2, comp->textures[TEXTURE_RGBA], 0, GL_FALSE, 0,
			   GL_WRITE_ONLY, GL_RGBA32UI);

	if (alpha) {
		glBindTexture(GL_TEXTURE_2D, comp->textures[TEXTURE_ALPHA]);
		glTexStorage2D(GL_TEXTURE_2D, 1, info->format, y_tex_width,
			       y_tex_height);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, y_tex_width,
				y_tex_height, GL_RGBA_INTEGER, info->type,
				alpha->y);
		glBindImageTexture(3, comp->textures[TEXTURE_ALPHA], 0,
				   GL_FALSE, 0, GL_READ_ONLY, info->format);
	}

	glBindTexture(GL_TEXTURE_2D, 0);

	program = comp->programs[PROGRAM_DETILE];
	glUseProgram(program);
	glUniform1ui(glGetUniformLocation(program, "tiles_per_stride"),
		     image->stride / MM21_TILE_WIDTH);
	glUniform1ui(glGetUniformLocation(program, "no_csc"), cli->no_csc);

	return (struct VariantData*) comp;

 error_surface:
	destroy_surface(comp->surface);
 error:
	free(comp);

	return NULL;
}

static void
destroy_comp(struct VariantData *data)
{
	struct CompData *comp = (struct CompData*) data;

	glDeleteFramebuffers(1, &comp->framebuffer);
	glDeleteTextures(TEXTURE_COUNT, comp->textures);
	glDeleteProgram(comp->programs[PROGRAM_DETILE]);
	destroy_surface(comp->surface);
	free(comp);
}

static int
run_comp(struct VariantData *data)
{
	struct CompData *comp = (struct CompData*) data;
	const struct PixelsPerThreadInfo *info =
		&ppt_info[comp->cli->variant - VARIANT_COMP_4];
	int x_groups = (comp->image->stride / info->count) / info->x_threads;
	int y_groups = comp->image->h_stride / info->y_threads;

	if (comp->cli->variant == VARIANT_COMP_8_2 ||
	    comp->cli->variant == VARIANT_COMP_16_2)
		y_groups /= 2;

	glDispatchCompute(x_groups, y_groups, 1);

	// Flush and sync with GPU to get decent timings.
	if (comp->cli->timing == TIMING_CPU)
		glFinish();

	return sigint;
}

static u8*
export_comp(struct VariantData *data)
{
	struct CompData *comp = (struct CompData*) data;
	u8 *pixels;

	pixels = malloc(comp->image->stride * comp->image->height * 4);
	assert(pixels);

	glBindFramebuffer(GL_FRAMEBUFFER, comp->framebuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			       GL_TEXTURE_2D, comp->textures[TEXTURE_RGBA], 0);
	glReadPixels(0, 0, comp->image->stride / 4, comp->image->height,
		     GL_RGBA_INTEGER, GL_UNSIGNED_INT, pixels);

	return pixels;
}

const struct VariantFuncs comp_funcs = {
	.create = create_comp,
	.destroy = destroy_comp,
	.run = run_comp,
	.export = export_comp
};
