# MM21 image format detiling tools.


## Usage

Create an optimised build:

```
  $ make
```

Create a debug build:

```
  $ make debug
```

Clean up the build (don't forget to do so to switch between builds):

```
  $ make clean
```

Test detiling variants:

```
  $ make check
```
