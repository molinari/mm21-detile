#version 310 es

// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include "common.glsl"

layout(location = 0) in vec2 v_tex_coord;

layout(location = 0) out vec4 color;

layout(location = 0) uniform uint tiles_per_stride;
layout(location = 1) uniform uint no_csc;

layout(binding = 0) uniform sampler2D y_tex;
layout(binding = 1) uniform sampler2D uv_tex;
layout(binding = 2) uniform sampler2D alpha_tex;

// Convert from linear coordinates to tiled coordinates.
// Y coords are returned in x and z, UV coords are returned in y and w.
ivec4
tiled_from_linear(ivec2 linear)
{
	// Constants:
	//   kTileW    = MM21_TILE_WIDTH                   =  16
	//   kYTileH   = MM21_Y_TILE_HEIGHT                =  32
	//   kUVTileH  = MM21_UV_TILE_HEIGHT               =  16
	//   kYTileSz  = kTileW * kYTileH                  = 512
	//   kUVTileSz = (kTileW / 2) * (kUVTileH / 2) * 2 = 128

	// 2D tile coord addressed by 'linear' in the linear buffer (1st tile is
	// (0,0), 2nd tile is (1,0), etc):
	//   tlc = linear.xy / (kTileW, kYTileH)
	uvec2 tlc = uvec2(linear) >> uvec2(4u, 5u);

	// 2D texel coord addressed by 'linear' in the tile:
	//   txc = linear.xy % (kTileW, kYTileH)
	uvec2 txc = uvec2(linear) & uvec2(15u, 31u);

	// Tile offset.
	uint tlo = tlc.y * tiles_per_stride + tlc.x;

	// Texel offset in Y and UV buffers:
	//   txo.x = (txc.y * kTileW) + txc.x
	//   txo.y = (((linear.y / 2) % kUVTileH)      * kTileW) + txc.x
	//         = (((linear.y      % kYTileH ) / 2) * kTileW) + txc.x
	//         = ((txc.y                      / 2) * kTileW) + txc.x
	uvec2 txo = (uvec2(txc.y, txc.y >> 1u) << uvec2(4u)) | txc.xx;

	// Global offset in Y and UV buffers:
	//   off.x = (tlo * kYTileSz) + txo.x
	//   off.y = (tlo * 2 * kUVTileSz) + txo.y
	uvec2 off = (uvec2(tlo) << uvec2(9u, 8u)) | txo;

	// XXX Hard-coded for images of width of 1920 for now!
	//
	// Convert to 2D coord in the tiled buffer:
	//   tiled.xy = off % (1920, 1920)
	//   tiled.zw = off / (1920, 1920)
	//
	// Move div/mod computation to floating-point pipe using reciprocal mul
	// as it's very slow otherwise on the integer pipe.
	vec2 q = vec2(off) * vec2(0.00052083333333333333);
	return ivec4(ivec2(fract(q) * vec2(1920.0, 960.0)), ivec2(q));

	// XXX Forgot why this is not needed in the compute shaders?
	// XXX The division by 2 has been moved above (mul by 1920 / 2).
	//
	// The UV texture size is half the Y texture size so the 2D coords are:
	//   uv_tiled.x = (off / 2) % (stride / 2)
	//   uv_tiled.y = (off / 2) / (stride / 2)
	// which can be simplified to:
	//   uv_tiled.x = (off % stride) / 2
	//   uv_tiled.y = off / stride
	//
	// tiled.y >>= 1u;
	// return ivec4(tiled);
}

// FIXME Based on Weston. Doesn't match ref.
vec3
convert(vec3 yuv)
{
	float y = 1.16438356 * (yuv.x - 0.0625);
	float u = yuv.y - 0.5;
	float v = yuv.z - 0.5;

	return clamp(vec3(y + (1.59602678 * v),
			  y - (0.39176229 * u) - (0.81296764 * v),
			  y + (2.01723214 * u)),
		     vec3(0.0), vec3(1.0));
}

// // FIXME Based on GStreamer's gstglcolorconvert. But doesn't match either.
// vec3
// convert(vec3 yuv)
// {
// 	// BT. 601 standard with the following ranges:
// 	// Y = [16..235] (of 255)
// 	// Cb/Cr = [16..240] (of 255)
// 	// const vec3 offset = vec3(-0.0625, -0.5,   -0.5);
// 	// const vec3 ycoeff = vec3( 1.164,   0.0,    1.596);
// 	// const vec3 ucoeff = vec3( 1.164,  -0.391, -0.813);
// 	// const vec3 vcoeff = vec3( 1.164,   2.018,  0.0);

// 	// BT. 709 standard with the following ranges:
// 	// Y = [16..235] (of 255)
// 	// Cb/Cr = [16..240] (of 255)
// 	const vec3 offset = vec3(-0.0625, -0.5,   -0.5);
// 	const vec3 ycoeff = vec3( 1.164,   0.0,    1.787);
// 	const vec3 ucoeff = vec3( 1.164,  -0.213, -0.531);
// 	const vec3 vcoeff = vec3( 1.164,   2.112,  0.0);

// 	yuv += offset;

// 	return vec3(dot(yuv, ycoeff), dot(yuv, ucoeff), dot(yuv, vcoeff));
// }

// vec3
// convert(vec3 yuv)
// {
// 	const float kr = 0.2126;
// 	const float kb = 0.0722;
// 	const float kg = 1.0 - kr - kb;

// 	// ecr = (cr - 128) * (255 / 224);
// 	// ecb = (cb - 128) * (255 / 224);
// 	// ey = (y - 16) * (255 / 219);
// 	float ecr = 1.1383928571428571 * yuv.z - 0.57142857142857142;
// 	float ecb = 1.1383928571428571 * yuv.y - 0.57142857142857142;
// 	float ey = 1.1643835616438356 * yuv.x - 0.073059360730593607;

// 	// r = (ecr * (2 * (1 - kr))) + ey;
// 	// g = (ey - kr * r - kb * b) / kg;
// 	// b = (ecb * (2 * (1 - kb))) + ey;
// 	float r = 1.5748 * ecr + ey;
// 	float b = 1.8556 * ecb + ey;
// 	float g = 1.3982102908277405 * ey - 0.29725950782997763 * r - 0.10095078299776286 * b;

//     // # Saturate to [0, 255] for out-of-range values.
//     // r = min(max(r, 0), 255)
//     // g = min(max(g, 0), 255)
//     // b = min(max(b, 0), 255)
// 	return vec3(r, g, b);
// }

void
main()
{
	ivec4 coord = tiled_from_linear(ivec2(v_tex_coord));
	vec3 yuv = vec3(texelFetch(y_tex,  coord.xz, 0).r,
			texelFetch(uv_tex, coord.yw, 0).rg);
	vec4 image = vec4(convert(vec3(texelFetch(y_tex,  coord.xz, 0).r,
				  texelFetch(uv_tex, coord.yw, 0).rg)), 1.0);

	if (no_csc == uint(0))
		color = vec4(convert(yuv), 1.0) * texelFetch(alpha_tex, coord.xz, 0).rrrr;
	else
		color = vec4(yuv, 1.0) * texelFetch(alpha_tex, coord.xz, 0).rrrr;
}
