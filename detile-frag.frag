#version 310 es

// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include "common.glsl"

layout(location = 0) in vec2 v_tex_coord;

layout(location = 0) out vec4 color;

layout(location = 0) uniform uint tiles_per_stride;
layout(location = 1) uniform uint no_csc;

layout(binding = 0) uniform sampler2D y_tex;
layout(binding = 1) uniform sampler2D uv_tex;

// Convert from linear coordinates to tiled coordinates.
// Y coords are returned in x and z, UV coords are returned in y and w.
ivec4
tiled_from_linear(ivec2 linear)
{
	// Constants:
	//   kTileW    = MM21_TILE_WIDTH                   =  16
	//   kYTileH   = MM21_Y_TILE_HEIGHT                =  32
	//   kUVTileH  = MM21_UV_TILE_HEIGHT               =  16
	//   kYTileSz  = kTileW * kYTileH                  = 512
	//   kUVTileSz = (kTileW / 2) * (kUVTileH / 2) * 2 = 128

	// 2D tile coord addressed by 'linear' in the linear buffer (1st tile is
	// (0,0), 2nd tile is (1,0), etc):
	//   tlc = linear.xy / (kTileW, kYTileH)
	uvec2 tlc = uvec2(linear) >> uvec2(4u, 5u);

	// 2D texel coord addressed by 'linear' in the tile:
	//   txc = linear.xy % (kTileW, kYTileH)
	uvec2 txc = uvec2(linear) & uvec2(15u, 31u);

	// Tile offset.
	uint tlo = tlc.y * tiles_per_stride + tlc.x;

	// Texel offset in Y and UV buffers:
	//   txo.x = (txc.y * kTileW) + txc.x
	//   txo.y = (((linear.y / 2) % kUVTileH)      * kTileW) + txc.x
	//         = (((linear.y      % kYTileH ) / 2) * kTileW) + txc.x
	//         = ((txc.y                      / 2) * kTileW) + txc.x
	uvec2 txo = (uvec2(txc.y, txc.y >> 1u) << uvec2(4u)) | txc.xx;

	// Global offset in Y and UV buffers:
	//   off.x = (tlo * kYTileSz) + txo.x
	//   off.y = (tlo * 2 * kUVTileSz) + txo.y
	uvec2 off = (uvec2(tlo) << uvec2(9u, 8u)) | txo;

	// XXX Hard-coded for images of width of 1920 for now!
	//
	// Convert to 2D coord in the tiled buffer:
	//   tiled.xy = off % (1920, 1920)
	//   tiled.zw = off / (1920, 1920)
	//
	// Move div/mod computation to floating-point pipe using reciprocal mul
	// as it's very slow otherwise on the integer pipe.
	vec2 q = vec2(off) * vec2(0.00052083333333333333);
	return ivec4(ivec2(fract(q) * vec2(1920.0, 960.0)), ivec2(q));

	// XXX Forgot why this is not needed in the compute shaders?
	// XXX The division by 2 has been moved above (mul by 1920 / 2).
	//
	// The UV texture size is half the Y texture size so the 2D coords are:
	//   uv_tiled.x = (off / 2) % (stride / 2)
	//   uv_tiled.y = (off / 2) / (stride / 2)
	// which can be simplified to:
	//   uv_tiled.x = (off % stride) / 2
	//   uv_tiled.y = off / stride
	//
	// tiled.y >>= 1u;
	// return ivec4(tiled);
}

// FIXME Based on Weston. Doesn't match ref.
vec3
convert(vec3 yuv)
{
	float y = 1.16438356 * (yuv.x - 0.0625);
	float u = yuv.y - 0.5;
	float v = yuv.z - 0.5;

	return clamp(vec3(y + (1.59602678 * v),
			  y - (0.39176229 * u) - (0.81296764 * v),
			  y + (2.01723214 * u)),
		     vec3(0.0), vec3(1.0));
}

void
main()
{
	ivec4 coord = tiled_from_linear(ivec2(v_tex_coord));
	vec3 yuv = vec3(texelFetch(y_tex,  coord.xz, 0).r,
			texelFetch(uv_tex, coord.yw, 0).rg);

	if (no_csc == uint(0))
		color = vec4(convert(yuv), 1.0);
	else
		color = vec4(yuv, 1.0);
}
