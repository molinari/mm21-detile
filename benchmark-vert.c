// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include "benchmark.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <linux/input.h>

enum TextureId {
	TEXTURE_Y = 0,
	TEXTURE_UV,
	TEXTURE_ALPHA,
	TEXTURE_RGBA,
	TEXTURE_COUNT
};

enum ProgramId {
	PROGRAM_DETILE = 0,
	PROGRAM_VIEW,
	PROGRAM_COUNT
};

struct Vertex {
	struct { float x, y; } pos;
	struct { float x, y_y, y_uv; } off;
};

struct VertData {
	struct SurfaceWayland *surface;
	unsigned int framebuffer;
	unsigned int programs[PROGRAM_COUNT];
	unsigned int textures[TEXTURE_COUNT];
	const struct CommandLine *cli;
	const struct Image *image;
	struct Vertex *vertices;
	unsigned short *indices;
	int n_vertices, n_indices;
	bool quit;
};

static void
handle_keyboard_key(struct SurfaceWayland *surface, uint32_t time, uint32_t key,
		    uint32_t state)
{
	struct VertData *data = surface->user_data;

	switch (key) {
	case KEY_Q:
	case KEY_ESC:
		data->quit = true;
		break;
	default:
		break;
	}
}

static void
handle_close(struct SurfaceWayland *surface)
{
	struct VertData *data = surface->user_data;

	data->quit = true;
}

static const struct SurfaceWaylandHandlers wayland_handlers = {
	.keyboard_key = handle_keyboard_key,
	.pointer_enter = NULL,
	.pointer_leave = NULL,
	.pointer_motion = NULL,
	.pointer_button = NULL,
	.pointer_axis = NULL,
	.resize = NULL,
	.close = handle_close
};

static struct VariantData*
create_vert(const struct CommandLine *cli, const struct Image *image,
	    const struct Image *alpha)
{
	int y_tex_width, y_tex_height, uv_tex_width, uv_tex_height;
	int max_texture_size;
	unsigned int program;
	struct VertData *vert;
	int n_tiles_w, n_tiles_h;
	struct Vertex *vtx;
	unsigned short *idx;

	vert = calloc(1, sizeof *vert);
	assert(vert);
	vert->cli = cli;
	vert->image = image;

	if (cli->view)
		vert->surface = create_surface_wayland(
			3, 1, image->width, image->height, WINDOW_TITLE,
			WINDOW_ID, &wayland_handlers, vert);
	else
		vert->surface = (struct SurfaceWayland*) create_surface(3, 1);
	if (!vert->surface)
		goto error;

	y_tex_width = image->stride;
	y_tex_height = image->h_stride;
	uv_tex_width = image->stride / 2;
	uv_tex_height = image->h_stride / 2;

	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_texture_size);
	if (y_tex_width > max_texture_size || y_tex_height > max_texture_size) {
		fprintf(stderr, "Error: Y texture size %dx%d higher than "
			"implementation limit %d.\n", y_tex_width, y_tex_height,
			max_texture_size);
		goto error_surface;
	}

	vert->programs[PROGRAM_DETILE] = compile_fragment_program(
		DETILE_VERT_VS_FILENAME, alpha ? DETILE_ALPHA_VERT_FS_FILENAME :
		DETILE_VERT_FS_FILENAME);
	if (!vert->programs[PROGRAM_DETILE])
		goto error_surface;

	glGenTextures(TEXTURE_COUNT, vert->textures);

	glGenFramebuffers(1, &vert->framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, vert->framebuffer);
	glBindTexture(GL_TEXTURE_2D, vert->textures[TEXTURE_RGBA]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, image->width, image->height);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			       GL_TEXTURE_2D, vert->textures[TEXTURE_RGBA], 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, vert->textures[TEXTURE_Y]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_R8, y_tex_width, y_tex_height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, y_tex_width, y_tex_height,
			GL_RED, GL_UNSIGNED_BYTE, image->y);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, vert->textures[TEXTURE_UV]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RG8, y_tex_width, y_tex_height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, uv_tex_width, uv_tex_height,
			GL_RG, GL_UNSIGNED_BYTE, image->uv);

	if (alpha) {
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, vert->textures[TEXTURE_ALPHA]);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_R8, y_tex_width,
			       y_tex_height);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, y_tex_width,
				y_tex_height, GL_RED, GL_UNSIGNED_BYTE,
				alpha->y);
	}

	program = vert->programs[PROGRAM_DETILE];
	glUseProgram(program);

	glUniform1ui(glGetUniformLocation(program, "no_csc"), cli->no_csc);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	glDisable(GL_BLEND);

	// Create mesh.
	n_tiles_w = (image->width / MM21_TILE_WIDTH) +
		!!(image->width % MM21_TILE_WIDTH);
	n_tiles_h = (image->height / MM21_UV_TILE_HEIGHT) +
		!!(image->height % MM21_UV_TILE_HEIGHT);
	vert->n_vertices = 4 * n_tiles_w * n_tiles_h;
	vert->n_indices = 6 * n_tiles_w * n_tiles_h;
	vert->vertices = vtx = malloc(vert->n_vertices * sizeof *vtx);
	vert->indices = idx = malloc(vert->n_indices * sizeof *idx);

	// Init mesh.
	for (int i = 0; i < n_tiles_h; i++) {
		float y1 = MIN(i * MM21_UV_TILE_HEIGHT, image->height);
		float y2 = MIN((i + 1) * MM21_UV_TILE_HEIGHT, image->height);
		int half_y_tile_size = MM21_Y_TILE_SIZE / 2;
		int odd = i & 1;
		for (int j = 0; j < n_tiles_w; j++) {
			size_t k = i * n_tiles_w + j;
			float x1 = MIN(j * MM21_TILE_WIDTH, image->width);
			float x2 = MIN((j + 1) * MM21_TILE_WIDTH, image->width);
			// Precomputed tile offsets.
			int tx = (int) x1 / MM21_TILE_WIDTH;
			int ty = (int) y1 / MM21_Y_TILE_HEIGHT;
			int t = ty * (image->stride / MM21_TILE_WIDTH) + tx;
			float t_y = t * MM21_Y_TILE_SIZE;
			float t_uv = t * 2 * MM21_UV_TILE_SIZE +
				odd * MM21_UV_TILE_SIZE;
			// Top/Left vertex.
			vtx[4*k + 0].pos.x = 2.0f * (x1 / image->width) - 1.0f;
			vtx[4*k + 0].pos.y = 2.0f * (y1 / image->height) - 1.0f;
			vtx[4*k + 0].off.x = 0.0f;
			vtx[4*k + 0].off.y_y = t_y + odd * half_y_tile_size;
			vtx[4*k + 0].off.y_uv = t_uv;
			// Bottom/Left vertex.
			vtx[4*k + 1].pos.x = 2.0f * (x1 / image->width) - 1.0f;
			vtx[4*k + 1].pos.y = 2.0f * (y2 / image->height) - 1.0f;
			vtx[4*k + 1].off.x = 0.0f;
			vtx[4*k + 1].off.y_y = t_y + half_y_tile_size +
				odd * half_y_tile_size;
			vtx[4*k + 1].off.y_uv = t_uv + MM21_UV_TILE_SIZE;
			// Top/Right vertex.
			vtx[4*k + 2].pos.x = 2.0f * (x2 / image->width) - 1.0f;
			vtx[4*k + 2].pos.y = 2.0f * (y1 / image->height) - 1.0f;
			vtx[4*k + 2].off.x = MM21_TILE_WIDTH;
			vtx[4*k + 2].off.y_y = t_y + odd * half_y_tile_size;
			vtx[4*k + 2].off.y_uv = t_uv;
			// Bottom/Right vertex.
			vtx[4*k + 3].pos.x = 2.0f * (x2 / image->width) - 1.0f;
			vtx[4*k + 3].pos.y = 2.0f * (y2 / image->height) - 1.0f;
			vtx[4*k + 3].off.x = MM21_TILE_WIDTH;
			vtx[4*k + 3].off.y_y = t_y + half_y_tile_size +
				odd * half_y_tile_size;
			vtx[4*k + 3].off.y_uv = t_uv + MM21_UV_TILE_SIZE;
			// Indices (triangles list).
			idx[6*k + 0] = 4*k + 0;
			idx[6*k + 1] = 4*k + 1;
			idx[6*k + 2] = 4*k + 2;
			idx[6*k + 3] = 4*k + 2;
			idx[6*k + 4] = 4*k + 1;
			idx[6*k + 5] = 4*k + 3;
		}
	}

	if (cli->view) {
		vert->programs[PROGRAM_VIEW] = compile_fragment_program(
			TEXTURE_VS_FILENAME, TEXTURE_FS_FILENAME);
		if (!vert->programs[PROGRAM_VIEW])
			goto error_surface;

		if (cli->fullscreen)
			xdg_toplevel_set_fullscreen(vert->surface->xdg_toplevel,
						    NULL);

		eglSwapInterval(vert->surface->egl.display, 0);
	}

	return (struct VariantData*) vert;

 error_surface:
	if (cli->view)
		destroy_surface_wayland(vert->surface);
	else
		destroy_surface((struct Surface*) vert->surface);
 error:
	free(vert);

	return NULL;
}

static void
destroy_vert(struct VariantData *data)
{
	struct VertData *vert = (struct VertData*) data;

	glDeleteFramebuffers(1, &vert->framebuffer);
	glDeleteTextures(TEXTURE_COUNT, vert->textures);
	glDeleteProgram(vert->programs[PROGRAM_DETILE]);
	free(vert->vertices);
	free(vert->indices);
	if (vert->cli->view) {
		glDeleteProgram(vert->programs[PROGRAM_VIEW]);
		destroy_surface_wayland(vert->surface);
	} else {
		destroy_surface((struct Surface*) vert->surface);
	}
	free(vert);
}

static int
run_vert(struct VariantData *data)
{
	struct VertData *vert = (struct VertData*) data;

	// Detile.
	glBindFramebuffer(GL_FRAMEBUFFER, vert->framebuffer);
	glViewport(0, 0, vert->image->width, vert->image->height);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, vert->textures[TEXTURE_Y]);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, vert->textures[TEXTURE_UV]);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, vert->textures[TEXTURE_ALPHA]);
	glUseProgram(vert->programs[PROGRAM_DETILE]);
	glVertexAttribPointer(0, 2, GL_FLOAT, 0, sizeof(struct Vertex),
			      &vert->vertices->pos);
	glVertexAttribPointer(1, 3, GL_FLOAT, 0, sizeof(struct Vertex),
			      &vert->vertices->off);
	glDrawElements(GL_TRIANGLES, vert->n_indices, GL_UNSIGNED_SHORT,
		       vert->indices);

	if (vert->cli->view) {
		struct SurfaceWayland *surface = vert->surface;
		float a = surface->buffer_size.width * vert->image->height;
		float b = surface->buffer_size.height * vert->image->width;
		float ratio_x = MIN(1.0f, b / a);
		float ratio_y = MIN(1.0f, a / b);
		struct { float x, y; } view_positions[4] = {
			{ -ratio_x,  ratio_y },
			{  ratio_x,  ratio_y },
			{ -ratio_x, -ratio_y },
			{  ratio_x, -ratio_y }
		};
		struct wl_region *region;

		// Draw.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, surface->buffer_size.width,
			   surface->buffer_size.height);
		glClear(GL_COLOR_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, vert->textures[TEXTURE_RGBA]);
		glUseProgram(vert->programs[PROGRAM_VIEW]);
		glVertexAttribPointer(0, 2, GL_FLOAT, 0, 0, view_positions);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		// Present.
		region = wl_compositor_create_region(surface->compositor);
		wl_region_add(region, 0, 0, INT32_MAX, INT32_MAX);
		wl_surface_set_opaque_region(surface->wl_surface, region);
		wl_region_destroy(region);
		eglSwapBuffers(surface->egl.display, surface->egl.surface);

		return dispatch_surface_wayland(vert->surface) || vert->quit ||
			sigint;
	} else {
		// Flush and sync with GPU to get decent timings.
		if (vert->cli->timing == TIMING_CPU)
			glFinish();

		return sigint;
	}
}

static u8*
export_vert(struct VariantData *data)
{
	struct VertData *vert = (struct VertData*) data;
	u8 *pixels;

	pixels = malloc(vert->image->stride * vert->image->height * 4);
	assert(pixels);

	glReadPixels(0, 0, vert->image->stride, vert->image->height, GL_RGBA,
		     GL_UNSIGNED_BYTE, pixels);

	return pixels;
}

const struct VariantFuncs vert_funcs = {
	.create = create_vert,
	.destroy = destroy_vert,
	.run = run_vert,
	.export = export_vert
};
