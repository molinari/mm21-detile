// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#ifndef BENCHMARK_H
#define BENCHMARK_H

#include "utils.h"

#define WINDOW_TITLE "MM21 Detile - Benchmark"
#define WINDOW_ID    "com.collabora.mm21-detile.benchmark"

#define TEXTURE_VS_FILENAME                 "texture.opt.vert"
#define TEXTURE_FS_FILENAME                 "texture.opt.frag"
#define DETILE_FRAG_FS_FILENAME             "detile-frag.opt.frag"
#define DETILE_ALPHA_FRAG_FS_FILENAME       "detile-alpha-frag.opt.frag"
#define DETILE_VERT_VS_FILENAME             "detile-vert.opt.vert"
#define DETILE_VERT_FS_FILENAME             "detile-vert.opt.frag"
#define DETILE_ALPHA_VERT_FS_FILENAME       "detile-alpha-vert.opt.frag"
#define DETILE_COMP_4_FS_FILENAME           "detile-comp-4.opt.comp"
#define DETILE_COMP_8_FS_FILENAME           "detile-comp-8.opt.comp"
#define DETILE_COMP_8_2_FS_FILENAME         "detile-comp-8-2.opt.comp"
#define DETILE_COMP_16_FS_FILENAME          "detile-comp-16.opt.comp"
#define DETILE_COMP_16_2_FS_FILENAME        "detile-comp-16-2.opt.comp"
#define DETILE_ALPHA_COMP_4_FS_FILENAME     "detile-alpha-comp-4.opt.comp"
#define DETILE_ALPHA_COMP_4_2_FS_FILENAME   "detile-alpha-comp-4-2.opt.comp"
#define DETILE_ALPHA_COMP_8_FS_FILENAME     "detile-alpha-comp-8.opt.comp"
#define DETILE_ALPHA_COMP_8_2_FS_FILENAME   "detile-alpha-comp-8-2.opt.comp"
#define DETILE_ALPHA_COMP_16_FS_FILENAME    "detile-alpha-comp-16.opt.comp"
#define DETILE_ALPHA_COMP_16_2_FS_FILENAME  "detile-alpha-comp-16-2.opt.comp"

#define TIMING_ITERATIONS_DEFAULT 1000

#define USAGE \
	"MM21 Benchmark\n" \
	"    Benchmark MM21 detiling variants.\n\n" \
	"Usage:\n" \
	"    mm21-benchmark [<options>]\n\n" \
	"Options:\n" \
	"    -h, --help\n" \
	"        Show this help.\n\n" \
	"    -i, --input <str>\n" \
	"        MM21 input image (" XSTR(INPUT_FILENAME_DEFAULT) " by default).\n\n" \
	"    -a, --alpha <str>\n" \
	"        MM21 alpha image (" XSTR(ALPHA_FILENAME_DEFAULT) " by default).\n" \
	"        Enable MM21 YUV + MM21 Y as alpha decoding.\n\n" \
	"    -w, --width <n>\n" \
	"        Width of input image (" XSTR(INPUT_WIDTH_DEFAULT) " by default).\n\n" \
	"    -H, --height <n>\n" \
	"        Height of input image (" XSTR(INPUT_HEIGHT_DEFAULT) " by default).\n\n" \
	"    -o, --output <str>\n" \
	"        Store RGBA output image to disk.\n\n" \
	"    -p, --png\n" \
	"        Store output image as PNG.\n\n" \
	"    -f, --force\n" \
	"        Overwrite output image if it exists.\n\n" \
	"    -v, --variant <str>\n" \
	"        frag:      Fragment shader detiling (default).\n" \
	"        vert:      Vertex shader detiling.\n" \
	"        comp-4:    Compute shader detiling 4 pixels per thread:\n" \
	"                   1 32-bit Y load, 1 32-bit UV load, 1 128-bit store.\n" \
	"        comp-8:    Compute shader detiling 8 pixels per thread:\n" \
	"                   1 64-bit Y load, 1 64-bit UV load, 2 128-bit stores.\n" \
	"        comp-8-2:  Compute shader detiling 8 pixels per thread (variant 2):\n" \
	"                   2 64-bit Y load, 1 64-bit UV load, 4 128-bit stores.\n" \
	"        comp-16:   Compute shader detiling 16 pixels per thread:\n" \
	"                   1 128-bit Y load, 1 128-bit UV load, 4 128-bit stores.\n" \
	"        comp-16-2: Compute shader detiling 16 pixels per thread (variant 2):\n" \
	"                   2 128-bit Y load, 1 128-bit UV load, 8 128-bit stores.\n" \
	"        copy:      Fragment shader copy. Source texture is linear (detiled\n" \
	"                   off-line). Useful for comparisons.\n\n" \
	"    -t, --timing <str>\n" \
	"        none:      Disable timing (default).\n" \
	"        cpu:       Monotonic CPU clock + GPU flush.\n" \
	"        gpu:       OpenGL ES disjoint timer query + GPU fence sync.\n\n" \
	"    -I, --iterations <n>\n" \
	"        Stop timing after <n> iterations without getting a lesser value\n" \
	"        (" XSTR(TIMING_ITERATIONS_DEFAULT) " by default).\n\n" \
	"    -V, --view\n" \
	"        Display output on-screen (if supported by variant).\n\n" \
	"    -F, --fullscreen\n" \
	"        Display output fullscreen.\n\n" \
	"    -n, --no-csc\n" \
	"        Disable colour-space conversion (YUV rendered as RGB).\n"

enum TimingId {
	TIMING_NONE = 0,
	TIMING_CPU,
	TIMING_GPU,
	TIMING_COUNT
};

enum VariantId {
	VARIANT_FRAG = 0,
	VARIANT_VERT,
	VARIANT_COMP_4,
	VARIANT_COMP_8,
	VARIANT_COMP_8_2,
	VARIANT_COMP_16,
	VARIANT_COMP_16_2,
	VARIANT_COPY,
	VARIANT_COUNT
};

struct CommandLine {
	const char *input;
	const char *alpha;
	const char *output;
	int width;
	int height;
	enum TimingId timing;
	enum VariantId variant;
	int iterations;
	bool png;
	bool force;
	bool view;
	bool fullscreen;
	bool no_csc;
};

struct VariantData {
	struct Surface *surface;
};

struct VariantFuncs {
	struct VariantData* (*create) (const struct CommandLine *cli,
				       const struct Image *image,
				       const struct Image *alpha);
	void                (*destroy)(struct VariantData *data);
	int                 (*run)    (struct VariantData *data);
	u8*                 (*export) (struct VariantData *data);
};

extern const struct VariantFuncs frag_funcs;
extern const struct VariantFuncs vert_funcs;
extern const struct VariantFuncs comp_funcs;
extern const struct VariantFuncs copy_funcs;

extern int sigint;

#endif  // BENCHMARK_H
