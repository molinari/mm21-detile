// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include "utils.h"
#include "common.h"
#include "xdg-shell-client-protocol.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <assert.h>
#include <math.h>
#include <linux/input.h>

#define DEBUG_Y_COORD  0
#define DEBUG_UV_COORD 1
#define DEBUG_Y_PLANE  2
#define DEBUG_U_PLANE  3
#define DEBUG_V_PLANE  4
#define DEBUG_OFF      5

#define VS_FILENAME "view.opt.vert"
#define FS_FILENAME "view.opt.frag"

#define WINDOW_TITLE "MM21 Detile - View"
#define WINDOW_ID    "com.collabora.mm21-detile.view"

#define SCALE_FACTOR    1.1f
#define SCALE_LEVEL_MAX 48.317715856f  // Log of 100 to base SCALE_FACTOR.

#define BILINEAR_DEFAULT 0

#define USAGE \
	"MM21 View\n" \
	"    View MM21 images.\n\n" \
	"    Zoom and pan image with pointer.\n" \
	"    Press 'b' to toggle bilinear filtering.\n" \
	"    Press 'd' to cycle between debug modes.\n" \
	"    Press 'q' to quit.\n\n" \
	"Usage:\n" \
	"    mm21-view [<options>]\n\n" \
	"Options:\n" \
	"    -h, --help\n" \
	"        Show this help.\n\n" \
	"    -i, --input <str>\n" \
	"        MM21 input image filename (" XSTR(INPUT_FILENAME_DEFAULT) ").\n\n" \
	"    -w, --width <n>\n" \
	"        Width of input image (" XSTR(INPUT_WIDTH_DEFAULT) ").\n\n" \
	"    -H, --height <n>\n" \
	"        Height of input image (" XSTR(INPUT_HEIGHT_DEFAULT) ").\n\n" \
	"    -f, --fullscreen\n" \
	"        Display output fullscreen.\n"

struct CommandLine {
	const char *input;
	int width;
	int height;
	bool fullscreen;
};

enum TextureId {
	TEXTURE_Y = 0,
	TEXTURE_UV,
	TEXTURE_COUNT
};

struct Application {
	struct SurfaceWayland *surface;
	const struct Image *image;

	unsigned int program;
	unsigned int textures[TEXTURE_COUNT];
	int debug_mode;

	float pointer_x;
	float pointer_y;
	float level;
	float scale;
	float tx;
	float ty;

	bool bilinear;
	bool pan;
	bool quit;
};

static bool g_sigint = false;

static int
init_command_line(struct CommandLine *cli, int argc, char **argv)
{
	int i = 0;
	char *end;

	cli->input = INPUT_FILENAME_DEFAULT;
	cli->width = INPUT_WIDTH_DEFAULT;
	cli->height = INPUT_HEIGHT_DEFAULT;
	cli->fullscreen = false;

	while (++i < argc) {
		if (!strcmp(argv[i], "-h") ||
		    !strcmp(argv[i], "--help")) {
			printf(USAGE);
			exit(0);
		} else if (!strcmp(argv[i], "-i") ||
			   !strcmp(argv[i], "--input")) {
			if (++i == argc) {
				fprintf(stderr, "Error: Invalid input "
					"filename.\n");
				return 1;
			}
			cli->input = argv[i];
		} else if (!strcmp(argv[i], "-w") ||
			   !strcmp(argv[i], "--width")) {
			if (++i == argc) {
				fprintf(stderr, "Error: Invalid input "
					"width.\n");
				return 1;
			}
			cli->width = strtol(argv[i], &end, 10);
			if (*end || errno == EINVAL || cli->width < 1) {
				fprintf(stderr, "Error: Invalid input "
					"width.\n");
				return 1;
			}
		} else if (!strcmp(argv[i], "-H") ||
			   !strcmp(argv[i], "--height")) {
			if (++i == argc) {
				fprintf(stderr, "Error: Invalid input "
					"height.\n");
				return 1;
			}
			cli->height = strtol(argv[i], &end, 10);
			if (*end || errno == EINVAL || cli->height < 1) {
				fprintf(stderr, "Error: Invalid input "
					"height.\n");
				return 1;
			}
		} else if (!strcmp(argv[i], "-f") ||
			   !strcmp(argv[i], "--fullscreen")) {
			cli->fullscreen = true;
		}
	}

	return 0;
}

void handle_pointer_enter(struct SurfaceWayland *surface, double x, double y)
{
	struct Application *app = surface->user_data;

	app->pointer_x = x;
	app->pointer_y = y;
}

void handle_pointer_motion(struct SurfaceWayland *surface, uint32_t time,
			   double x, double y)
{
	struct Application *app = surface->user_data;
	struct Geometry *buffer_size = &app->surface->buffer_size;
	float a, b, ratio_x, ratio_y, sx, sy, dx, dy;

	if (app->pan) {
		a = buffer_size->width * app->image->height;
		b = buffer_size->height * app->image->width;
		ratio_x = a / MIN(a, b);
		ratio_y = b / MIN(a, b);
		sx = ratio_x * app->scale;
		sy = ratio_y * app->scale;
		dx = (app->pointer_x - x) / buffer_size->width;
		dy = (app->pointer_y - y) / buffer_size->height;

		app->tx = CLAMP(app->tx + sx * dx, 0.0f, 1.0f - app->scale);
		app->ty = CLAMP(app->ty + sy * dy, 0.0f, 1.0f - app->scale);
	}

	app->pointer_x = x;
	app->pointer_y = y;
}

void handle_pointer_button(struct SurfaceWayland *surface, uint32_t time,
			   uint32_t button, uint32_t state)
{
	struct Application *app = surface->user_data;

	app->pan = button == BTN_LEFT &&
		state == WL_POINTER_BUTTON_STATE_PRESSED;
}

void handle_pointer_axis(struct SurfaceWayland *surface, uint32_t time,
			 uint32_t axis, int value)
{
	struct Application *app = surface->user_data;
	struct Geometry *buffer_size = &app->surface->buffer_size;
	float a, b, ratio_x, ratio_y, sx, sy;
	float s, ds;

	// FIXME Computations here are correct but the Wayland cursor isn't
	//       correctly aligned so zooming feels incorrect.

	if (axis !=WL_POINTER_AXIS_VERTICAL_SCROLL)
		return;

	app->level += value > 0 ? -1 : 1;
	app->level = CLAMP(app->level, 0, SCALE_LEVEL_MAX);

	a = buffer_size->width * app->image->height;
	b = buffer_size->height * app->image->width;
	ratio_x = a / MIN(a, b);
	ratio_y = b / MIN(a, b);
	sx = app->pointer_x / buffer_size->width;
	sy = app->pointer_y / buffer_size->height;
	sx = (sx - 0.5f) * ratio_x + 0.5f;
	sy = (sy - 0.5f) * ratio_y + 0.5f;
	sx = CLAMP(app->tx + sx * app->scale, 0.0f, 1.0f);
	sy = CLAMP(app->ty + sy * app->scale, 0.0f, 1.0f);
	sx = (sx - app->tx) / app->scale;
	sy = (sy - app->ty) / app->scale;
	s = powf(SCALE_FACTOR, -app->level);
	ds = app->scale - s;

	app->scale = s;
	app->tx = CLAMP(app->tx + sx * ds, 0.0f, 1.0f - s);
	app->ty = CLAMP(app->ty + sy * ds, 0.0f, 1.0f - s);
}

void handle_keyboard_key(struct SurfaceWayland *surface, uint32_t time,
			 uint32_t key, uint32_t state)
{
	struct Application *app = surface->user_data;
	int loc;

	switch (key) {
	case KEY_B:
		app->bilinear = !app->bilinear;
		loc = glGetUniformLocation(app->program, "bilinear");
		glUniform1i(loc, app->bilinear);
		break;

	case KEY_D:
		app->debug_mode = (app->debug_mode + 1) % (DEBUG_OFF + 1);
		loc = glGetUniformLocation(app->program, "debug_mode");
		glUniform1i(loc, app->debug_mode);
		break;

	case KEY_Q:
	case KEY_ESC:
		app->quit = true;
		break;

	default: break;
	}
}

void handle_resize(struct SurfaceWayland *surface,
		   const struct Geometry geometry)
{
	struct Application *app = surface->user_data;

	glViewport(0, 0, geometry.width, geometry.height);
	glUniform2f(glGetUniformLocation(app->program, "viewport_size"),
		    geometry.width, geometry.height);
}

void handle_close(struct SurfaceWayland *surface)
{
	struct Application *app = surface->user_data;

	app->quit = true;
}

static const struct SurfaceWaylandHandlers wayland_handlers = {
	.keyboard_key = handle_keyboard_key,
	.pointer_enter = handle_pointer_enter,
	.pointer_leave = NULL,
	.pointer_motion = handle_pointer_motion,
	.pointer_button = handle_pointer_button,
	.pointer_axis = handle_pointer_axis,
	.resize = handle_resize,
	.close = handle_close
};

static int
init_application(struct Application *app, const struct CommandLine *cli,
		 const struct Image *image)
{
	int y_tex_width, y_tex_height, uv_tex_width, uv_tex_height;
	int max_texture_size;

	memset(app, 0, sizeof *app);
	app->image = image;
	app->pointer_x = -1;
	app->pointer_y = -1;
	app->scale = 1.0f;
	app->bilinear = BILINEAR_DEFAULT;
	app->debug_mode = DEBUG_OFF;

	app->surface = create_surface_wayland(
		3, 1, image->width, image->height, WINDOW_TITLE, WINDOW_ID,
		&wayland_handlers, app);
	if (!app->surface)
		goto error;

	y_tex_width = image->stride;
	y_tex_height = image->h_stride;
	uv_tex_width = image->stride / 2;
	uv_tex_height = image->h_stride / 2;

#if !defined(NDEBUG)
	printf("Image size:      %dx%d\n"
	       "Y texture size:  %dx%d\n"
	       "UV texture size: %dx%d\n",
	       image->width, image->height,
	       y_tex_width, y_tex_height,
	       uv_tex_width, uv_tex_height);
#endif

	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_texture_size);
	if (y_tex_width > max_texture_size || y_tex_height > max_texture_size) {
		fprintf(stderr, "Error: Y texture size %dx%d higher than "
			"implementation limit %d.\n", y_tex_width, y_tex_height,
			max_texture_size);
		goto error_surface;
	}

	app->program = compile_fragment_program(VS_FILENAME, FS_FILENAME);
	if (!app->program)
		goto error_surface;
	glUseProgram(app->program);

	glGenTextures(TEXTURE_COUNT, app->textures);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, app->textures[TEXTURE_Y]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_R8, y_tex_width, y_tex_height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, y_tex_width, y_tex_height,
			GL_RED, GL_UNSIGNED_BYTE, image->y);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, app->textures[TEXTURE_UV]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RG8, y_tex_width, y_tex_height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, uv_tex_width, uv_tex_height,
			GL_RG, GL_UNSIGNED_BYTE, image->uv);

	glUniform2f(glGetUniformLocation(app->program, "viewport_size"),
		    app->surface->buffer_size.width,
		    app->surface->buffer_size.height);
	glUniform2f(glGetUniformLocation(app->program, "image_size"),
		    image->width, image->height);
	glUniform2i(glGetUniformLocation(app->program, "y_tex_size"),
		    y_tex_width, y_tex_height);
	glUniform2i(glGetUniformLocation(app->program, "uv_tex_size"),
		    uv_tex_width, uv_tex_height);
	glUniform1i(glGetUniformLocation(app->program, "bilinear"),
		    app->bilinear);
	glUniform1i(glGetUniformLocation(app->program, "debug_mode"),
		    app->debug_mode);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	eglSwapInterval(app->surface->egl.display, 1);

	if (cli->fullscreen)
		xdg_toplevel_set_fullscreen(app->surface->xdg_toplevel, NULL);

	return 0;

 error_surface:
	destroy_surface_wayland(app->surface);
 error:
	return 1;
}

static void
destroy_application(struct Application *app)
{
	glDeleteTextures(TEXTURE_COUNT, app->textures);
	glDeleteProgram(app->program);
	destroy_surface_wayland(app->surface);
}

static void
render(const struct Application *app, const struct Image *image)
{
	static const struct { float x, y; } positions[4] = {
		{ -1.0f, -1.0f },
		{  1.0f, -1.0f },
		{ -1.0f,  1.0f },
		{  1.0f,  1.0f }
	};
	struct Geometry *buffer_size = &app->surface->buffer_size;
	float s = app->scale;
	float tx = app->tx;
	float ty = app->ty;
	float a = buffer_size->width * image->height;
	float b = buffer_size->height * image->width;
	float ratio_x = 0.5f * MAX(1.0f, a / b);
	float ratio_y = 0.5f * MAX(1.0f, b / a);
	const struct { float s, t; } tex_coords[4] = {
		{ tx + (0.5f - ratio_x) * s, ty + (0.5f + ratio_y) * s },
		{ tx + (0.5f + ratio_x) * s, ty + (0.5f + ratio_y) * s },
		{ tx + (0.5f - ratio_x) * s, ty + (0.5f - ratio_y) * s },
		{ tx + (0.5f + ratio_x) * s, ty + (0.5f - ratio_y) * s }
	};
	struct wl_region *region;

	glVertexAttribPointer(0, 2, GL_FLOAT, 0, 0, positions);
	glVertexAttribPointer(1, 2, GL_FLOAT, 0, 0, tex_coords);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	region = wl_compositor_create_region(app->surface->compositor);
	wl_region_add(region, 0, 0, INT32_MAX, INT32_MAX);
	wl_surface_set_opaque_region(app->surface->wl_surface, region);
	wl_region_destroy(region);
	eglSwapBuffers(app->surface->egl.display, app->surface->egl.surface);
}

static void
signal_handler(int signum)
{
	g_sigint = true;
}

int
main(int argc, char *argv[])
{
	struct sigaction signal;
	struct CommandLine cli;
	struct Image image;
	struct Application app;

	signal.sa_handler = signal_handler;
	sigemptyset(&signal.sa_mask);
	signal.sa_flags = SA_RESETHAND;
	sigaction(SIGINT, &signal, NULL);

	if (init_command_line(&cli, argc, argv))
		return 1;

	if (load_image(&image, cli.input, cli.width, cli.height))
		return 1;

	if (init_application(&app, &cli, &image)) {
		destroy_image(&image);
		return 1;
	}

	while (!app.quit && !g_sigint &&
	       !dispatch_surface_wayland(app.surface))
		render(&app, &image);

	destroy_application(&app);
	destroy_image(&image);

	return 0;
}
