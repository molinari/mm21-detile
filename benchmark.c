// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include "benchmark.h"
#include "common.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STBI_WRITE_NO_STDIO
#include "stb_image_write.h"

#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>

static const struct VariantFuncs *variant_funcs[] = {
	&frag_funcs,
	&vert_funcs,
	&comp_funcs,
	&comp_funcs,
	&comp_funcs,
	&comp_funcs,
	&comp_funcs,
	&copy_funcs
};

int sigint = 0;

static void
signal_handler(int)
{
	sigint = 1;
}

static int
init_command_line(struct CommandLine *cli, int argc, char **argv)
{
	int i = 0;
	char *end;

	cli->input = INPUT_FILENAME_DEFAULT;
	cli->alpha = NULL;
	cli->output = NULL;
	cli->width = INPUT_WIDTH_DEFAULT;
	cli->height = INPUT_HEIGHT_DEFAULT;
	cli->timing = TIMING_NONE;
	cli->variant = VARIANT_FRAG;
	cli->iterations = TIMING_ITERATIONS_DEFAULT;
	cli->png = false;
	cli->force = false;
	cli->view = false;
	cli->fullscreen = false;
	cli->no_csc = false;

	while (++i < argc) {
		if (!strcmp(argv[i], "-h") ||
		    !strcmp(argv[i], "--help")) {
			printf(USAGE);
			exit(0);
		} else if (!strcmp(argv[i], "-i") ||
			   !strcmp(argv[i], "--input")) {
			if (++i == argc) {
				fprintf(stderr, "Error: Invalid input "
					"filename.\n");
				return 1;
			}
			cli->input = argv[i];
		} else if (!strcmp(argv[i], "-a") ||
			   !strcmp(argv[i], "--alpha")) {
			if (++i == argc) {
				fprintf(stderr, "Error: Invalid input "
					"filename.\n");
				return 1;
			}
			cli->alpha = argv[i];
		} else if (!strcmp(argv[i], "-w") ||
			   !strcmp(argv[i], "--width")) {
			if (++i == argc) {
				fprintf(stderr, "Error: Invalid input "
					"width.\n");
				return 1;
			}
			cli->width = strtol(argv[i], &end, 10);
			if (*end || errno == EINVAL || cli->width < 1) {
				fprintf(stderr, "Error: Invalid input "
					"width.\n");
				return 1;
			}
		} else if (!strcmp(argv[i], "-H") ||
			   !strcmp(argv[i], "--height")) {
			if (++i == argc) {
				fprintf(stderr, "Error: Invalid input "
					"height.\n");
				return 1;
			}
			cli->height = strtol(argv[i], &end, 10);
			if (*end || errno == EINVAL || cli->height < 1) {
				fprintf(stderr, "Error: Invalid input "
					"height.\n");
				return 1;
			}
		} else if (!strcmp(argv[i], "-o") ||
			   !strcmp(argv[i], "--output")) {
			if (++i == argc) {
				fprintf(stderr, "Error: Invalid output "
					"filename.\n");
				return 1;
			}
			cli->output = argv[i];
		} else if (!strcmp(argv[i], "-p") ||
			   !strcmp(argv[i], "--png")) {
			cli->png = true;
		} else if (!strcmp(argv[i], "-f") ||
			   !strcmp(argv[i], "--force")) {
			cli->force = true;
		} else if (!strcmp(argv[i], "-v") ||
			   !strcmp(argv[i], "--variant")) {
			if (++i == argc) {
				fprintf(stderr, "Error: Invalid variant.\n");
				return 1;
			}
			if (!strcmp(argv[i], "frag")) {
				cli->variant = VARIANT_FRAG;
			} else if (!strcmp(argv[i], "vert")) {
				cli->variant = VARIANT_VERT;
			} else if (!strcmp(argv[i], "comp-4")) {
				cli->variant = VARIANT_COMP_4;
			} else if (!strcmp(argv[i], "comp-8")) {
				cli->variant = VARIANT_COMP_8;
			} else if (!strcmp(argv[i], "comp-8-2")) {
				cli->variant = VARIANT_COMP_8_2;
			} else if (!strcmp(argv[i], "comp-16")) {
				cli->variant = VARIANT_COMP_16;
			} else if (!strcmp(argv[i], "comp-16-2")) {
				cli->variant = VARIANT_COMP_16_2;
			} else if (!strcmp(argv[i], "copy")) {
				cli->variant = VARIANT_COPY;
			} else {
				fprintf(stderr, "Error: Invalid variant.\n");
				return 1;
			}
		} else if (!strcmp(argv[i], "-t") ||
			   !strcmp(argv[i], "--timing")) {
			if (++i == argc) {
				fprintf(stderr, "Error: Invalid timing.\n");
				return 1;
			}
			if (!strcmp(argv[i], "none")) {
				cli->timing = TIMING_NONE;
			} else if (!strcmp(argv[i], "cpu")) {
				cli->timing = TIMING_CPU;
			} else if (!strcmp(argv[i], "gpu")) {
				cli->timing = TIMING_GPU;
			} else {
				fprintf(stderr, "Error: Invalid timing.\n");
				return 1;
			}
		} else if (!strcmp(argv[i], "-I") ||
			   !strcmp(argv[i], "--iterations")) {
			if (++i == argc) {
				fprintf(stderr, "Error: Invalid iterations.\n");
				return 1;
			}
			cli->iterations = strtol(argv[i], &end, 10);
			if (*end || errno == EINVAL || cli->iterations < 1) {
				fprintf(stderr, "Error: Invalid iterations.\n");
				return 1;
			}
		} else if (!strcmp(argv[i], "-V") ||
			   !strcmp(argv[i], "--view")) {
			cli->view = true;
		} else if (!strcmp(argv[i], "-F") ||
			   !strcmp(argv[i], "--fullscreen")) {
			cli->fullscreen = true;
		} else if (!strcmp(argv[i], "-n") ||
			   !strcmp(argv[i], "--no-csc")) {
			cli->no_csc = true;
		}
	}

	// Could be supported but variants would need to detile again to get a
	// proper read-back from the renderbuffer.
	if (cli->view && cli->output) {
		fprintf(stderr, "Error: view and export options are mutually "
			"exclusive.\n");
		return 1;
	}

	return 0;
}

struct WriteInfo {
	struct CommandLine *cli;
	int fd;
	int error;
};

static void
write_image(void *context, void *pixels, int size)
{
	struct WriteInfo *info = (struct WriteInfo*) context;

	if (write(info->fd, pixels, size) != size) {
		fprintf(stderr, "Error: Cannot write output file \"%s\": %s.\n",
			info->cli->output, strerror(errno));
		info->error = 1;
	}
}

static int
export_image(int width, int height, int stride, struct CommandLine *cli,
	     struct VariantData *data)
{
	struct stat st;
	int fd, size, error = 0;
	u8 *pixels;

	if (!cli->force && stat(cli->output, &st) == 0) {
		fprintf(stderr, "Error: Output file \"%s\" exists. Use option "
			"-f to overwrite.\n", cli->output);
		return 1;
	}
	fd = open(cli->output, O_WRONLY | O_CREAT, 0644);
	if (fd < 0) {
		fprintf(stderr, "Error: Cannot open output file \"%s\": %s.\n",
			cli->output, strerror(errno));
		return 1;
	}

	pixels = variant_funcs[cli->variant]->export(data);
	if (cli->png) {
		struct WriteInfo info = { cli, fd, 0 };
		stbi_write_png_to_func(write_image, &info, width, height,
				       4, pixels, stride * 4);
		error = info.error;
	} else {
		size = height * stride * 4;
		if (write(fd, pixels, size) != size) {
			fprintf(stderr, "Error: Cannot write output file "
				"\"%s\": %s.\n", cli->output, strerror(errno));
			error = 1;
		}
	}

	free(pixels);
	close(fd);

	return error;
}

int
main(int argc, char *argv[])
{
	struct sigaction signal;
	struct CommandLine cli;
	struct Image image;
	struct Image alpha;
	struct VariantData *data;
	int error = 0;

	signal.sa_handler = signal_handler;
	sigemptyset(&signal.sa_mask);
	signal.sa_flags = SA_RESETHAND;
	sigaction(SIGINT, &signal, NULL);

	if (init_command_line(&cli, argc, argv))
		return 1;

	if (load_image(&image, cli.input, cli.width, cli.height))
		return 1;

	if (cli.alpha && load_image(&alpha, cli.alpha, cli.width, cli.height))
		return 1;

	// Create.
	data = variant_funcs[cli.variant]->create(&cli, &image,
						  cli.alpha ? &alpha : NULL);
	if (!data) {
		destroy_image(&image);
		if (cli.alpha)
			destroy_image(&alpha);
		return 1;
	}

	// Run.
	switch (cli.timing) {
	case TIMING_NONE: {
		if (cli.view)
			while (!variant_funcs[cli.variant]->run(data));
		else
			variant_funcs[cli.variant]->run(data);
		break;
	}
	case TIMING_CPU: {
		u64 elapsed = elapsed_time_cpu(
			cli.iterations,
			(TaskFunc) variant_funcs[cli.variant]->run, data);
		if (elapsed)
			printf("CPU timing: %.3f ms\n", elapsed * 0.000001);
		else
			error = 1;
		break;
	}
	case TIMING_GPU: {
		u64 elapsed = elapsed_time_gpu(
			data->surface->display, cli.iterations,
			(TaskFunc) variant_funcs[cli.variant]->run, data);
		if (elapsed)
			printf("GPU timing: %.3f ms\n", elapsed * 0.000001);
		else
			error = 1;
		break;
	}
	default:
		assert("Not reached!");
	};

	// Export.
	if (cli.output && !error)
		if (export_image(image.width, image.height, image.stride, &cli,
				 data))
			error = 1;

	// Destroy.
	variant_funcs[cli.variant]->destroy(data);

	destroy_image(&image);
	if (cli.alpha)
		destroy_image(&alpha);

	return error;
}
