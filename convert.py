#!/usr/bin/env python3

# SPDX-FileCopyrightText:  © 2024 Collabora Ltd
# SPDX-License-Identifier: MIT

import os
import sys

from dataclasses import dataclass
from enum import Enum, auto
from struct import unpack


# MM21 tiled 4:2:0 Y'CbCr format. 1 luma plane, 1 colour difference (chroma)
# plane with Cb/Cr components interleaved. Luma and chroma tiles have different
# heights.
MM21_TILE_WIDTH     = 16
MM21_Y_TILE_HEIGHT  = 32
MM21_UV_TILE_HEIGHT = 16
MM21_Y_TILE_SIZE    = MM21_TILE_WIDTH * MM21_Y_TILE_HEIGHT
MM21_UV_TILE_SIZE   = (MM21_TILE_WIDTH // 2) * (MM21_UV_TILE_HEIGHT // 2) * 2

# Default image parameters.
INPUT_DEFAULT  = 'image.rgba'
OUTPUT_DEFAULT = 'image.mm21'
WIDTH_DEFAULT  = 1920
HEIGHT_DEFAULT = 1080

# Recommendation ITU-R BT.601-7, section 2.5.1.
BT_601_7_KR = 0.2990
BT_601_7_KB = 0.1140

# Recommendation ITU-R BT.709-6, section 3.2.
BT_709_6_KR = 0.2126
BT_709_6_KB = 0.0722

class Encoding(Enum):
    BT_601_7 = auto()
    BT_709_6 = auto()

@dataclass
class Coord:
    x: int = 0
    y: int = 0


# Convert from full range 8-bit R'G'B' data to 8-bit Y'CbCr.
def ycbcr_from_rgb(r, g, b, encoding = Encoding.BT_709_6):
    assert 0 <= r <= 255 and 0 <= g <= 255 and 0 <= b <= 255

    # Basic implementation based on ITU-R recommendations for reference.

    # Normalised coefficients.
    if encoding == Encoding.BT_601_7:
        kr = BT_601_7_KR
        kb = BT_601_7_KB
    elif encoding == Encoding.BT_709_6:
        kr = BT_709_6_KR
        kb = BT_709_6_KB
    assert 0 < kr + kb < 1
    kg = 1 - kr - kb

    # Luma is a weighted arithmetic mean of the gamma-corrected R'G'B'
    # components.
    ey = kr * r + kg * g + kb * b

    # Chroma is the colour differences between red/blue and luma. Differences
    # must be re-normalised to the original range (see ITU-R BT.601-7, section
    # 2.5.2 for an explanation lacking in subsequent standard).
    ecr = (r - ey) / (2 * (1 - kr))
    ecb = (b - ey) / (2 * (1 - kb))

    # Both ITU-R BT.601-7 and BT.709-6 recommendations quantize 8-bit luma to
    # [16, 235] and 8-bit chroma to [16, 240]. Rounding to nearest integer with
    # ties towards positive infinity is required, hence the use of int(x + 0.5)
    # instead of round().
    bias = 16
    peak_y = 235
    peak_cbcr = 240
    y = int(((ey * ((peak_y - bias) / 255)) + bias) + 0.5)
    cr = int(((ecr * ((peak_cbcr - bias) / 255)) + 128) + 0.5)
    cb = int(((ecb * ((peak_cbcr - bias) / 255)) + 128) + 0.5)

    # Derivation of the commonly used equations (often extracted as is from
    # "Video Demystified, 4th Ed - Chapter 3 - YCbCr Color Space - Computer
    # Systems Considerations"). The derived floating-point coefficients are
    # rounded to 17 significant digits so they can be converted to IEEE 754
    # binary64 with the best precision.
    #
    # ITU-R BT.601-7:
    #
    #  Normalised coefficients:
    #   kr = 0.299
    #   kb = 0.114
    #   kg = 1 - kr - kb = 0.587
    #
    #  Luma and chroma:
    #   ey  = kr * r + kg * g + kb * b
    #       = 0.299 * r + 0.587 * g + 0.114 * b
    #   ecr = (r - ey) / (2 * (1 - kr))
    #       = (r - 0.299 * r - 0.587 * g - 0.114 * b) / 1.402
    #       = (    0.701 * r - 0.587 * g - 0.114 * b) / 1.402
    #       = 0.5 * r - (0.587 / 1.402) * g - (0.114 / 1.402) * b
    #   ecb = (b - ey) / (2 * (1 - kb))
    #       = (b - 0.299 * r - 0.587 * g - 0.114 * b) / 1.772
    #       = (  - 0.299 * r - 0.587 * g + 0.886 * b) / 1.772
    #       = - (0.299 / 1.772) * r - (0.587 / 1.772) * g + 0.5 * b
    #
    #  8-bit quantization (no rounding):
    #   bias      = 16
    #   peak_y    = 235
    #   peak_cbcr = 240
    #   y         = (ey * ((peak_y - bias) / 255)) + bias
    #             = ((0.299 * r + 0.587 * g + 0.114 * b) * (219 / 255)) + 16
    #             ~ 0.25678823529411764 * r + 0.50412941176470588 * g + 0.097905882352941176 * b + 16
    #   cr        = (ecr * ((peak_cbcr - bias) / 255)) + 128
    #             = ((0.5 * r - (0.587 / 1.402) * g - (0.114 / 1.402) * b) * (224 / 255)) + 128
    #             ~ 0.4392156862745098 * r - 0.36778831361360521 * g - 0.07142737266090459 * b + 128
    #   cb        = (ecb * ((peak_cbcr - bias) / 255)) + 128
    #             = ((- (0.299 / 1.772) * r - (0.587 / 1.772) * g + 0.5 * b) * (224 / 255)) + 128
    #             ~ - 0.14822290089850839 * r - 0.29099278537600142 * g + 0.4392156862745098 * b + 128
    #
    # ITU-R BT.709-6:
    #
    #  Normalised coefficients:
    #   kr = 0.2126
    #   kb = 0.0722
    #   kg = 1 - kr - kb = 0.7152
    #
    #  Luma and chroma:
    #   ey  = kr * r + kg * g + kb * b
    #       = 0.2126 * r + 0.7152 * g + 0.0722 * b
    #   ecr = (r - ey) / (2 * (1 - kr))
    #       = (r - 0.2126 * r - 0.7152 * g - 0.0722 * b) / 1.5748
    #       = (    0.7874 * r - 0.7152 * g - 0.0722 * b) / 1.5748
    #       = 0.5 * r - (0.7152 / 1.5748) * g - (0.0722 / 1.5748) * b
    #   ecb = (b - ey) / (2 * (1 - kb))
    #       = (b - 0.2126 * r - 0.7152 * g - 0.0722 * b) / 1.8556
    #       = (  - 0.2126 * r - 0.7152 * g + 0.9278 * b) / 1.8556
    #       = - (0.2126 / 1.8556) * r - (0.7152 / 1.8556) * g + 0.5 * b
    #
    #  8-bit quantization (no rounding):
    #   bias      = 16
    #   peak_y    = 235
    #   peak_cbcr = 240
    #   y         = (ey * ((peak_y - bias) / 255)) + bias
    #             = ((0.2126 * r + 0.7152 * g + 0.0722 * b) * (219 / 255)) + 16
    #             ~ 0.18258588235294118 * r + 0.61423058823529412 * g + 0.062007058823529412 * b + 16
    #   cr        = (ecr * ((peak_cbcr - bias) / 255)) + 128
    #             = ((0.5 * r - (0.7152 / 1.5748) * g - (0.0722 / 1.5748) * b) * (224 / 255)) + 128
    #             ~ 0.4392156862745098 * r - 0.39894216259020753 * g - 0.04027352368430227 * b + 128
    #   cb        = (ecb * ((peak_cbcr - bias) / 255)) + 128
    #             = ((- (0.2126 / 1.8556) * r - (0.7152 / 1.8556) * g + 0.5 * b) * (224 / 255)) + 128
    #             ~ - 0.10064373237978097 * r - 0.33857195389472883 * g + 0.4392156862745098 * b + 128

    return y, cb, cr


# Convert pixel from full range 8-bit R'G'B' to 8-bit Y' using ITU-R BT.601-7.
def y_from_rgb_bt601(p):
    # Based on equations derived in ycbcr_from_rgb().
    y_r, y_g, y_b = 0.25678823529411764, 0.50412941176470588, 0.097905882352941176
    r, g, b = p & 0xff, (p >> 8) & 0xff, (p >> 16) & 0xff
    return int(y_r * r + y_g * g + y_b * b + 16.5)


# Convert pixel from full range 8-bit R'G'B' to 8-bit CbCr using ITU-R BT.601-7.
def cbcr_from_rgb_bt601(p):
    # Based on equations derived in ycbcr_from_rgb().
    cb_r, cb_g, cb_b = -0.14822290089850839, -0.29099278537600142, 0.4392156862745098
    cr_r, cr_g, cr_b = 0.4392156862745098, -0.36778831361360521, -0.07142737266090459
    r, g, b = p & 0xff, (p >> 8) & 0xff, (p >> 16) & 0xff
    return (int(cb_r * r + cb_g * g + cb_b * b + 128.5), \
            int(cr_r * r + cr_g * g + cr_b * b + 128.5))


# Convert pixel from full range 8-bit R'G'B' to 8-bit Y' using ITU-R BT.709-6.
def y_from_rgb_bt709(p):
    # Based on equations derived in ycbcr_from_rgb().
    y_r, y_g, y_b = 0.18258588235294118, 0.61423058823529412, 0.062007058823529412
    r, g, b = p & 0xff, (p >> 8) & 0xff, (p >> 16) & 0xff
    return int(y_r * r + y_g * g + y_b * b + 16.5)


# Convert pixel from full range 8-bit R'G'B' to 8-bit CbCr using ITU-R BT.709-6.
def cbcr_from_rgb_bt709(p):
    # Based on equations derived in ycbcr_from_rgb().
    cb_r, cb_g, cb_b = -0.10064373237978097, -0.33857195389472883, 0.4392156862745098
    cr_r, cr_g, cr_b = 0.4392156862745098, -0.39894216259020753, -0.04027352368430227
    r, g, b = p & 0xff, (p >> 8) & 0xff, (p >> 16) & 0xff
    return (int(cb_r * r + cb_g * g + cb_b * b + 128.5), \
            int(cr_r * r + cr_g * g + cr_b * b + 128.5))


# Convert from 8-bit Y'CbCr data to full range 8-bit R'G'B'.
def rgb_from_ycbcr(y, cb, cr, encoding = Encoding.BT_709_6):
    # Luma is in [16, 235] and chroma in [16, 240] but out-of-range values are
    # allowed and must be handled appropriately (see saturation step below).
    assert 0 <= y <= 255 and 0 <= cb <= 255 and 0 <= cr <= 255

    # Basic implementation inverting ITU-R recommendations for reference.

    # Normalised coefficients.
    if encoding == Encoding.BT_601_7:
        kr = BT_601_7_KR
        kb = BT_601_7_KB
    elif encoding == Encoding.BT_709_6:
        kr = BT_709_6_KR
        kb = BT_709_6_KB
    assert 0 < kr + kb < 1
    kg = 1 - kr - kb

    # De-quantize 8-bit luma from [16, 235] and 8-bit chroma from [16, 240].
    bias = 16
    peak_y = 235
    peak_cbcr = 240
    ecr = (cr - 128) * (255 / (peak_cbcr - bias))
    ecb = (cb - 128) * (255 / (peak_cbcr - bias))
    ey = (y - bias) * (255 / (peak_y - bias))

    # R'G'B' inverse transforms.
    r = int(((ecr * (2 * (1 - kr))) + ey) + 0.5)
    b = int(((ecb * (2 * (1 - kb))) + ey) + 0.5)
    g = int(((ey - kr * r - kb * b) / kg) + 0.5)

    # Saturate to [0, 255] for out-of-range values.
    r = min(max(r, 0), 255)
    g = min(max(g, 0), 255)
    b = min(max(b, 0), 255)

    # Derivation of the commonly used equations (often extracted as is from
    # "Video Demystified, 4th Ed - Chapter 3 - YCbCr Color Space - Computer
    # Systems Considerations"). The derived floating-point coefficients are
    # rounded to 17 significant digits so they can be converted to IEEE 754
    # binary64 with the best precision.
    #
    # ITU-R BT.601-7:
    #
    #  Normalised coefficients:
    #   kr = 0.299
    #   kb = 0.114
    #   kg = 1 - kr - kb = 0.587
    #
    #  8-bit de-quantization:
    #   bias      = 16
    #   peak_y    = 235
    #   peak_cbcr = 240
    #   ecr       = (cr - 128) * (255 / (peak_cbcr - bias))
    #             = (cr - 128) * (255 / 224)
    #   ecb       = (cb - 128) * (255 / (peak_cbcr - bias))
    #             = (cb - 128) * (255 / 224)
    #   ey        = (y - bias) * (255 / (peak_y - bias))
    #             = (y - 16) * (255 / 219)
    #
    #  Inverse transforms (no rounding, no saturation):
    #   r = (ecr * (2 * (1 - kr))) + ey
    #     = (((cr - 128) * (255 / 224)) * 1.402) + ((y - 16) * (255 / 219))
    #     ~ 1.1643835616438356 * (y - 16) + 1.5960267857142857 * (cr - 128)
    #   b = (ecb * (2 * (1 - kb))) + ey
    #     = (((cb - 128) * (255 / 224)) * 1.772) + ((y - 16) * (255 / 219))
    #     ~ 1.1643835616438356 * (y - 16) + 2.0172321428571428 * (cb - 128)
    #   g = (ey - kr * r - kb * b) / kg
    #     = ((y - 16) * (255 / 219)) / 0.587 -
    #       (0.299 * ((((cr - 128) * (255 / 224)) * 1.402) + ((y - 16) * (255 / 219)))) / 0.587 -
    #       (0.114 * ((((cb - 128) * (255 / 224)) * 1.772) + ((y - 16) * (255 / 219)))) / 0.587
    #     = (y  -  16) * (255 / 128.553) -
    #       (y  -  16) * ((0.299 * 255) / (0.587 * 219)) -
    #       (y  -  16) * ((0.114 * 255) / (0.587 * 219)) -
    #       (cr - 128) * ((0.299 * 1.402 * 255) / (0.587 * 224)) -
    #       (cb - 128) * ((0.114 * 1.772 * 255) / (0.587 * 224))
    #     ~ 1.1643835616438356 * (y - 16) - 0.81296764723777075 * (cr - 128) - 0.3917622900949136 * (cb - 128)
    #
    # ITU-R BT.709-6:
    #
    #  Normalised coefficients:
    #   kr = 0.2126
    #   kb = 0.0722
    #   kg = 1 - kr - kb = 0.7152
    #
    #  8-bit de-quantization:
    #   Same as ITU-R BT.601-7 8-bit de-quantization. See above.
    #
    #  Inverse transforms (no rounding, no saturation):
    #   r = (ecr * (2 * (1 - kr))) + ey
    #     = (((cr - 128) * (255 / 224)) * 1.5742) + ((y - 16) * (255 / 219))
    #     ~ 1.1643835616438356 * (y - 16) + 1.7920580357142857 * (cr - 128)
    #   b = (ecb * (2 * (1 - kb))) + ey
    #     = (((cb - 128) * (255 / 224)) * 1.8556) + ((y - 16) * (255 / 219))
    #     ~ 1.1643835616438356 * (y - 16) + 2.1124017857142857 * (cb - 128)
    #   g = (ey - kr * r - kb * b) / kg
    #     = ((y - 16) * (255 / 219)) / 0.7152 -
    #       (0.2126 * ((((cr - 128) * (255 / 224)) * 1.5742) + ((y - 16) * (255 / 219)))) / 0.7152 -
    #       (0.0722 * ((((cb - 128) * (255 / 224)) * 1.8556) + ((y - 16) * (255 / 219)))) / 0.7152
    #     = (y  -  16) * (255 / 156.6288) -
    #       (y  -  16) * ((0.2126 * 255) / (0.7152 * 219)) -
    #       (y  -  16) * ((0.0722 * 255) / (0.7152 * 219)) -
    #       (cr - 128) * ((0.2126 * 1.5742 * 255) / (0.7152 * 224)) -
    #       (cb - 128) * ((0.0722 * 1.8556 * 255) / (0.7152 * 224))
    #     ~ 1.1643835616438356 * (y - 16) - 0.53270628969918504 * (cr - 128) - 0.21324861427372962 * (cb - 128)

    return r, g, b


# Convert from linear coordinates to byte offsets in the Y' and CbCr buffers.
def tiled_from_linear(linear, stride):
    # Constants:
    #   kTileW    = MM21_TILE_WIDTH                   =  16
    #   kYTileH   = MM21_Y_TILE_HEIGHT                =  32
    #   kUVTileH  = MM21_UV_TILE_HEIGHT               =  16
    #   kYTileSz  = kTileW * kYTileH                  = 512
    #   kUVTileSz = (kTileW / 2) * (kUVTileH / 2) * 2 = 128

    # tiles_per_stride = stride // kTileW
    tiles_per_stride = stride >> 4

    # 2D tile coord addressed by 'linear' in the linear buffer (1st tile is
    # (0,0), 2nd tile is (1,0), etc):
    #   tlc_x, tlc_y = linear.x // kTileW, linear.y // kYTileH
    tlc_x, tlc_y = linear.x >> 4, linear.y >> 5

    # 2D texel coord addressed by 'linear' in the tile:
    #   txc_x, txc_y = linear.x % kTileW, linear.y % kYTileH
    txc_x, txc_y = linear.x & 15, linear.y & 31

    # Tile offset.
    tlo = tlc_y * tiles_per_stride + tlc_x

    # Texel offset in Y' and CbCr buffers:
    #   txo_y    = (txc_y * kTileW) + txc_x
    #   txo_cbcr = (((linear.y // 2) % kUVTileH)       * kTileW) + txc_x
    #            = (((linear.y       % kYTileH ) // 2) * kTileW) + txc_x
    #            = ((txc_y                       // 2) * kTileW) + txc_x
    txo_y, txo_cbcr = (txc_y << 4) | txc_x, ((txc_y >> 1) << 4) | txc_x

    # Global offset in Y and UV buffers:
    #   off_y = (tlo * kYTileSz) + txo.x
    #   off_cbcr = (tlo * 2 * kUVTileSz) + txo.y
    off_y, off_cbcr = (tlo << 9) | txo_y, (tlo << 8) | txo_cbcr

    return off_y, off_cbcr


def usage():
    print(f'MM21 Convert\n'
          f'    Convert RGBA image to MM21.\n\n'
          f'Usage:\n'
          f'    convert.py [<options>]\n\n'
          f'Options:\n'
          f'    -h, --help\n'
          f'        Show this help.\n\n'
          f'    -i, --input <str>\n'
          f'        RGBA input image ("{INPUT_DEFAULT}" by default).\n\n'
          f'    -w, --width <n>\n'
          f'        Width of input image ({WIDTH_DEFAULT} by default).\n\n'
          f'    -H, --height <n>\n'
          f'        Height of input image ({HEIGHT_DEFAULT} by default).\n\n'
          f'    -o, --output <str>\n'
          f'        MM21 output image ("{OUTPUT_DEFAULT}" by default).\n')


def from_str(string, func, default = 0):
    try:
        return func(string)
    except:
        return default


def parse_cli(args):
    input_name = INPUT_DEFAULT
    output_name = OUTPUT_DEFAULT
    width = WIDTH_DEFAULT
    height = HEIGHT_DEFAULT
    args_iterator: Iterator[int] = iter(range(1, len(args)))

    for i in args_iterator:
        arg = args[i]
        skip_next = False
        if arg in ('-h', '--help'):
            usage()
            sys.exit(0)
        elif arg in ('-i', '--input'):
            input_name = i + 1 < len(args) and args[i + 1] or ''
            skip_next = True
        elif arg in ('-w', '--width'):
            width = i + 1 < len(args) and from_str(args[i + 1], int, width)
            skip_next = True
        elif arg in ('-H', '--height'):
            height = i + 1 < len(args) and from_str(args[i + 1], int, height)
            skip_next = True
        elif arg in ('-o', '--output'):
            output_name = i + 1 < len(args) and args[i + 1] or ''
            skip_next = True
        skip_next and i + 1 < len(args) and next(args_iterator)

    return input_name, width, height, output_name


def open_files(input_name, output_name):
    try:
        input_file = open(input_name, 'rb')
    except OSError:
        print(f'Error: Cannot open input file \'{input_name}\'.')
        return 1

    try:
        output_file = open(output_name, 'wb')
    except OSError:
        print(f'Error: Cannot open output file \'{output_name}\'.')
        return 1

    return (input_file, output_file)


# Round up to the next power of 2.
def round_up_pow2(a, pow2):
    assert a > 0
    assert pow2 > 0 and (pow2 - 1) & pow2 == 0  # Is power of 2?
    return (a + (pow2 - 1)) & ~(pow2 - 1)


# Load 8-bit R'G'B'A linear image file and store 8-bit Y'CbCr tiled image file.
# Could be more efficient but we don't really care here.
def convert(rgba_file, width, height, mm21_file):
    # MM21 frame layout.
    w_stride = round_up_pow2(width, MM21_TILE_WIDTH);
    h_stride = round_up_pow2(height, MM21_Y_TILE_HEIGHT);
    nw = w_stride // MM21_TILE_WIDTH;
    nh = h_stride // MM21_Y_TILE_HEIGHT;
    y_size = nw * nh * MM21_Y_TILE_SIZE;
    nh = h_stride // MM21_UV_TILE_HEIGHT;
    cbcr_size = nw * nh * MM21_UV_TILE_SIZE;
    size = y_size + cbcr_size;

    mm21 = bytearray(size)
    rgba = rgba_file.read()
    file_size = len(rgba)
    rgba_size = width * height * 4
    if file_size != rgba_size:
        print(f'Error: Invalid RGBA input image, must be {rgba_size} bytes not '
              f'{file_size}.')
        return 1
    rgba = unpack('I' * (rgba_size // 4), rgba)

    for j in range(0, height, 2):
        for i in range(0, width, 2):
            c00 = tiled_from_linear(Coord(i, j), w_stride)
            c10 = tiled_from_linear(Coord(i + 1, j), w_stride)
            c01 = tiled_from_linear(Coord(i, j + 1), w_stride)
            c11 = tiled_from_linear(Coord(i + 1, j + 1), w_stride)

            # Convert from R'G'B' and store tiled luma plane.
            k = j * width + i
            mm21[c00[0]] = y_from_rgb_bt601(rgba[k])
            mm21[c10[0]] = y_from_rgb_bt601(rgba[k + 1])
            mm21[c01[0]] = y_from_rgb_bt601(rgba[k + width])
            mm21[c11[0]] = y_from_rgb_bt601(rgba[k + width + 1])

            # Convert from R'G'B' and store tiled chroma plane with 4:2:0
            # subsampling.
            c = c00[1] + y_size
            mm21[c], mm21[c + 1] = cbcr_from_rgb_bt601(rgba[k])

    mm21_file.write(mm21)


def main(args):
    input_name, width, height, output_name = parse_cli(args)

    files = open_files(input_name, output_name)
    if files == 1:
        return 1

    if convert(files[0], width, height, files[1]):
        return 1

    files[0].close()
    files[1].close()

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
