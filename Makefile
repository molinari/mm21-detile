# SPDX-FileCopyrightText:  © 2024 Collabora Ltd
# SPDX-License-Identifier: MIT

SHELL             := /bin/bash

BIN_REQ           := python3 \
                     pkg-config \
                     glslc \
                     spirv-opt \
                     spirv-cross \
                     gst-launch-1.0

WL_PROTOCOLS_DIR  := $(shell pkg-config wayland-protocols --variable=pkgdatadir)
WL_SCANNER        := $(shell pkg-config --variable=wayland_scanner wayland-scanner)

IMAGE_PATTERN     := 24  # Colors
ALPHA_PATTERN     := 21  # Pinwheel
IMAGE_WIDTH       := 1920
IMAGE_HEIGHT      := 1080
IMAGE_RGBA        := image.rgba
IMAGE_MM21        := image.mm21
ALPHA_RGBA        := alpha.rgba
ALPHA_MM21        := alpha.mm21

LIBS              := egl glesv2 wayland-client wayland-egl
CFLAGS            := -I. -D_GNU_SOURCE $(shell pkg-config --cflags $(LIBS)) -Wall
LDFLAGS           := $(shell pkg-config --libs $(LIBS)) -lm
VIEW              := mm21-view
VIEW_OBJS         := xdg-shell-protocol.o \
                     utils.o \
                     view.o
VIEW_SHADERS      := view.opt.vert \
                     view.opt.frag
BENCHMARK         := mm21-benchmark
BENCHMARK_OBJS    := xdg-shell-protocol.o \
                     utils.o \
                     benchmark.o \
                     benchmark-frag.o \
                     benchmark-vert.o \
                     benchmark-comp.o \
                     benchmark-copy.o
BENCHMARK_SHADERS := texture.opt.vert \
                     texture.opt.frag \
                     detile-frag.opt.frag \
                     detile-alpha-frag.opt.frag \
                     detile-vert.opt.vert \
                     detile-vert.opt.frag \
                     detile-alpha-vert.opt.frag \
                     detile-comp-4.opt.comp \
                     detile-comp-8.opt.comp \
                     detile-comp-8-2.opt.comp \
                     detile-comp-16.opt.comp \
                     detile-comp-16-2.opt.comp \
                     detile-alpha-comp-4.opt.comp \
                     detile-alpha-comp-4-2.opt.comp \
                     detile-alpha-comp-8.opt.comp \
                     detile-alpha-comp-8-2.opt.comp \
                     detile-alpha-comp-16.opt.comp \
                     detile-alpha-comp-16-2.opt.comp

$(foreach bin, $(BIN_REQ), \
	$(if $(shell command -v $(bin) 2> /dev/null),, \
		$(error Cannot find '$(bin)' in PATH)))

all:   optim
optim: CFLAGS += -DNDEBUG -O3
debug: CFLAGS += -Werror -g -Og
optim: $(VIEW) $(BENCHMARK)
debug: $(VIEW) $(BENCHMARK)

%.o: %.c utils.h
	@ echo "CC	$@"
	@ $(CC) -std=c11 $(CFLAGS) -c $< -o $@

.PRECIOUS: %-client-protocol.h %-protocol.c
%-client-protocol.h: $(WL_PROTOCOLS_DIR)/**/*/%.xml
	@ echo "GEN	$@"
	@ $(WL_SCANNER) client-header $< $@
%-protocol.c: $(WL_PROTOCOLS_DIR)/**/*/%.xml %-client-protocol.h
	@ echo "GEN	$@"
	@ $(WL_SCANNER) private-code $< $@

$(VIEW): $(VIEW_OBJS) $(VIEW_SHADERS) $(IMAGE_MM21) $(ALPHA_MM21)
	@ echo "LD	$@"
	@ $(CC) $(VIEW_OBJS) -o $@ $(LDFLAGS)

$(BENCHMARK): $(BENCHMARK_OBJS) $(BENCHMARK_SHADERS) $(IMAGE_MM21) $(ALPHA_MM21)
	@ echo "LD	$@"
	@ $(CC) $(BENCHMARK_OBJS) -o $@ $(LDFLAGS)

%.opt.vert: %.vert common.glsl
	@ echo "OPT	$@"
	@ glslc -c $< --target-env=opengl -o $<.spv || exit 1
	@ spirv-opt -O $<.spv -o $<.opt.spv || exit 2
	@ spirv-cross $<.opt.spv --output $@ || exit 3
	@ rm -f $<.spv $<.opt.spv
%.opt.frag: %.frag common.glsl
	@ echo "OPT	$@"
	@ glslc -c $< --target-env=opengl -o $<.spv || exit 1
	@ spirv-opt -O $<.spv -o $<.opt.spv || exit 2
	@ spirv-cross $<.opt.spv --output $@ || exit 3
	@ rm -f $<.spv $<.opt.spv
%.opt.comp: %.comp common.glsl
	@ echo "OPT	$@"
	@ glslc -c $< --target-env=opengl -o $<.spv || exit 1
	@ spirv-opt -O $<.spv -o $<.opt.spv || exit 2
	@ spirv-cross $<.opt.spv --output $@ || exit 3
	@ rm -f $<.spv $<.opt.spv

# The patterns created by GStreamer's videotestsrc element don't have the exact
# same colors when generated for RGB or YCbCr formats. For instance, the SMPTE
# +Q area is blue with RGBA and purple with YCbCr. This isn't a mm21-detile
# format conversion issue.
$(IMAGE_RGBA):
	@ echo "GEN	$@"
	@ $(shell gst-launch-1.0 -q \
		videotestsrc num-buffers=1 pattern=$(IMAGE_PATTERN) ! \
		video/x-raw,format=RGBA,width=$(IMAGE_WIDTH),height=$(IMAGE_HEIGHT) ! \
		filesink location=$@)
$(IMAGE_MM21): $(IMAGE_RGBA)
	@ echo "GEN	$@"
	@ ./convert.py --input $(IMAGE_RGBA) --output $@ --width $(IMAGE_WIDTH) --height $(IMAGE_HEIGHT)
$(ALPHA_RGBA):
	@ echo "GEN	$@"
	@ $(shell gst-launch-1.0 -q \
		videotestsrc num-buffers=1 pattern=$(ALPHA_PATTERN) ! \
		video/x-raw,format=RGBA,width=$(IMAGE_WIDTH),height=$(IMAGE_HEIGHT) ! \
		filesink location=$@)
$(ALPHA_MM21): $(ALPHA_RGBA)
	@ echo "GEN	$@"
	@ ./convert.py --input $(ALPHA_RGBA) --output $@ --width $(IMAGE_WIDTH) --height $(IMAGE_HEIGHT)

check: $(BENCHMARK) $(IMAGE_MM21) $(ALPHA_MM21)
	@ declare -a variants=("copy" "frag" "comp-4" "comp-8" "comp-16"); \
	for i in "$${variants[@]}"; do \
		echo "TEST	$(BENCHMARK) --variant $$i"; \
		./$(BENCHMARK) --variant $$i --output /tmp/mm21-benchmark-test.rgba || exit 1; \
		./compare.py --width $(IMAGE_WIDTH) --height $(IMAGE_HEIGHT) \
			--mm21 $(IMAGE_MM21) --rgba /tmp/mm21-benchmark-test.rgba || exit 2; \
	done

perf: $(BENCHMARK) $(IMAGE_MM21)
	@ ./$(BENCHMARK) --variant copy --timing gpu --iterations 1 > /dev/null 2>&1; \
	declare -i has_gpu_timing=$$?; \
	declare -a variants=("copy" "frag" "vert" "comp-4" "comp-8" "comp-8-2" "comp-16" "comp-16-2"); \
	for i in "$${variants[@]}"; do \
		if (( has_gpu_timing == 0 )); then \
			echo "$(BENCHMARK) --variant $$i --timing gpu"; \
			./$(BENCHMARK) --variant $$i --timing gpu || exit 2; \
		else \
			echo "$(BENCHMARK) --variant $$i --timing cpu"; \
			./$(BENCHMARK) --variant $$i --timing cpu || exit 1; \
		fi \
	done

perf-alpha: $(BENCHMARK) $(IMAGE_MM21) $(ALPHA_MM21)
	@ ./$(BENCHMARK) --variant copy --timing gpu --iterations 1 > /dev/null 2>&1; \
	declare -i has_gpu_timing=$$?; \
	declare -a variants=("copy" "frag" "vert" "comp-4" "comp-8" "comp-8-2" "comp-16" "comp-16-2"); \
	for i in "$${variants[@]}"; do \
		if (( has_gpu_timing == 0 )); then \
			echo "$(BENCHMARK) --alpha $(ALPHA_MM21) --variant $$i --timing gpu"; \
			./$(BENCHMARK) --alpha $(ALPHA_MM21) --variant $$i --timing gpu || exit 2; \
		else \
			echo "$(BENCHMARK) --alpha $(ALPHA_MM21) --variant $$i --timing cpu"; \
			./$(BENCHMARK) --alpha $(ALPHA_MM21) --variant $$i --timing cpu || exit 1; \
		fi \
	done

perf-no-csc: $(BENCHMARK) $(IMAGE_MM21)
	@ ./$(BENCHMARK) --no-csc --variant copy --timing gpu --iterations 1 > /dev/null 2>&1; \
	declare -i has_gpu_timing=$$?; \
	declare -a variants=("copy" "frag" "vert" "comp-4" "comp-8" "comp-8-2" "comp-16" "comp-16-2"); \
	for i in "$${variants[@]}"; do \
		if (( has_gpu_timing == 0 )); then \
			echo "$(BENCHMARK) --no-csc --variant $$i --timing gpu"; \
			./$(BENCHMARK) --no-csc --variant $$i --timing gpu || exit 2; \
		else \
			echo "$(BENCHMARK) --no-csc --variant $$i --timing cpu"; \
			./$(BENCHMARK) --no-csc --variant $$i --timing cpu || exit 1; \
		fi \
	done

.PHONY:
clean:
	@ for i in \
		$(VIEW) \
		$(VIEW_OBJS) \
		$(VIEW_SHADERS) \
		$(BENCHMARK) \
		$(BENCHMARK_OBJS) \
		$(BENCHMARK_SHADERS) \
		$(IMAGE_RGBA) \
		$(IMAGE_MM21) \
		$(ALPHA_RGBA) \
		$(ALPHA_MM21) \
		image-*.png \
		*-protocol.[h,c]; do \
		if [ -e "$$i" ]; then \
			echo "RM	$$i"; \
			rm -f "$$i"; \
		fi \
	done
