// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <time.h>
#include <poll.h>
#include <assert.h>

#include "utils.h"

struct Output {
	struct wl_output *wl_output;
	struct SurfaceWayland *surface;
	unsigned int name;
	struct wl_list link;  // struct Application::output_list
	enum wl_output_transform transform;
	int scale;
};

struct WindowOutput {
	struct Output *output;
	struct wl_list link;  // struct Application::window_output_list
};

void*
load_file(const char *filename, int *size, bool add_null)
{
	int fd;
	struct stat st;
	char *data;

	fd = open(filename, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "Error: Cannot open \"%s\".\n", filename);
		return NULL;
	}
	if (fstat(fd, &st) < 0) {
		fprintf(stderr, "Error: Cannot stat \"%s\".\n", filename);
		close(fd);
		return NULL;
	}
	if (st.st_size == 0) {
		fprintf(stderr, "Error: Empty file \"%s\".\n", filename);
		close(fd);
		return NULL;
	}
	data = aligned_alloc(4096, ROUND_UP_N(st.st_size + add_null, 4096));
	if (!data) {
		fprintf(stderr, "Error: Cannot allocate buffer for \"%s\".\n",
			filename);
		close(fd);
		return NULL;
	}
	if (read(fd, data, st.st_size) != st.st_size) {
		fprintf(stderr, "Error: Cannot read file \"%s\".\n", filename);
		close(fd);
		free(data);
		return NULL;
	}
	close(fd);

	if (add_null)
		data[st.st_size] = '\0';

	*size = st.st_size + add_null;

	return data;
}

int
load_image(struct Image *image, const char *filename, int width, int height)
{
	char *dump;
	int file_size, w_stride, h_stride, nw, nh, y_size, uv_size, size;
	int gst_size;

	dump = load_file(filename, &file_size, false);
	if (!dump)
		return 1;

	// MM21 frame layout.
	w_stride = ROUND_UP_N(width, MM21_TILE_WIDTH);
	h_stride = ROUND_UP_N(height, MM21_Y_TILE_HEIGHT);
	nw = w_stride / MM21_TILE_WIDTH;
	nh = h_stride / MM21_Y_TILE_HEIGHT;
	y_size = nw * nh * MM21_Y_TILE_SIZE;
	nh = h_stride / MM21_UV_TILE_HEIGHT;
	uv_size = nw * nh * MM21_UV_TILE_SIZE;
	size = y_size + uv_size;

	// Fix to load GStreamer images with format NV12_16L32S before commit
	// fb02516b (16/02/24) which used an invalid interleaved UV tile size.
	if (file_size != size) {
		nh = h_stride / MM21_Y_TILE_HEIGHT;
		uv_size = nw * nh * MM21_Y_TILE_SIZE;
		gst_size = y_size + uv_size;
		if (file_size != gst_size) {
			fprintf(stderr, "Error: Invalid MM21 image, must be %d "
				"bytes not %d.\n", size, file_size);
			free(dump);
			return 1;
		}
	}

	image->width = width;
	image->height = height;
	image->stride = w_stride;
	image->h_stride = h_stride;
	image->y = dump;
	image->uv = dump + y_size;

	return 0;
}

// FIXME Just filling green pixels for now.
void*
detile_image(const struct Image *image)
{
	u32* pixels = malloc(4 * image->width * image->height);
	assert(pixels);

	for (int i = 0; i < image->height; i++)
		for (int j = 0; j < image->width; j++)
			pixels[image->stride * i + j] = 0xff00ff00;

	return pixels;
}

void
destroy_image(struct Image *image)
{
	free(image->y);
}

bool
check_extension(const char *extensions, const char *extension)
{
	size_t length = strlen(extension);
	const char *end = extensions + strlen(extensions);

	while (extensions < end) {
		size_t n = 0;

		if (*extensions == ' ') {
			extensions++;
			continue;
		}
		n = strcspn(extensions, " ");
		if (n == length && !strncmp(extension, extensions, n))
			return true;
		extensions += n;
	}

	return false;
}

void*
get_egl_proc_address(const char *address)
{
	const char *extensions = eglQueryString(EGL_NO_DISPLAY, EGL_EXTENSIONS);

	if (extensions &&
	    (check_extension(extensions, "EGL_EXT_platform_wayland") ||
	     check_extension(extensions, "EGL_KHR_platform_wayland")))
		return (void*) eglGetProcAddress(address);

	return NULL;
}

EGLDisplay
get_egl_display(EGLenum platform, void *native_display,
		const EGLint *attributes)
{
	static PFNEGLGETPLATFORMDISPLAYEXTPROC get_platform_display = NULL;

	if (!get_platform_display)
		get_platform_display = (PFNEGLGETPLATFORMDISPLAYEXTPROC)
			get_egl_proc_address("eglGetPlatformDisplayEXT");

	if (get_platform_display)
		return get_platform_display(platform, native_display,
					    attributes);

	return eglGetDisplay(native_display);
}

int
compile_fragment_program(const char *vs_filename, const char *fs_filename)
{
	char *vs_source, *fs_source;
	unsigned int vs, fs, program;
	char log[4096];
	int size, success;

	vs_source = load_file(vs_filename, &size, true);
	if (!vs_source)
		return 0;
	vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, (const char**) &vs_source, NULL);
	glCompileShader(vs);
	glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vs, ARRAY_LENGTH(log), NULL, log);
		fprintf(stderr, "Error: Cannot compile vertex shader.\n%s",
			log);
		glDeleteShader(vs);
		free(vs_source);
		return 0;
	}
	free(vs_source);

	fs_source = load_file(fs_filename, &size, true);
	if (!fs_source)
		return 0;
	fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, (const char**) &fs_source, NULL);
	glCompileShader(fs);
	glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fs, ARRAY_LENGTH(log), NULL, log);
		fprintf(stderr, "Error: Cannot compile fragment shader.\n%s",
			log);
		glDeleteShader(vs);
		glDeleteShader(fs);
		free(fs_source);
		return 0;
	}
	free(fs_source);

	program = glCreateProgram();
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(program, ARRAY_LENGTH(log), NULL, log);
		fprintf(stderr, "Error: Cannot link program.\n%s", log);
		glDeleteShader(vs);
		glDeleteShader(fs);
		return 0;
	}

	glDeleteShader(vs);
	glDeleteShader(fs);

	return program;
}

int
compile_compute_program(const char *cs_filename)
{
	char *source;
	unsigned int cs, program;
	char log[4096];
	int size, success;

	source = load_file(cs_filename, &size, true);
	if (!source)
		return 0;
	cs = glCreateShader(GL_COMPUTE_SHADER);
	glShaderSource(cs, 1, (const char**) &source, NULL);
	glCompileShader(cs);
	glGetShaderiv(cs, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(cs, ARRAY_LENGTH(log), NULL, log);
		fprintf(stderr, "Error: Cannot compile compute shader.\n%s",
			log);
		glDeleteShader(cs);
		free(source);
		return 0;
	}
	free(source);

	program = glCreateProgram();
	glAttachShader(program, cs);
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(program, ARRAY_LENGTH(log), NULL, log);
		fprintf(stderr, "Error: Cannot link program.\n%s", log);
		glDeleteShader(cs);
		return 0;
	}

	glDeleteShader(cs);

	return program;
}

u64
elapsed_time_cpu(int iterations, TaskFunc func, void *data)
{
	struct timespec t1, t2;
	int quit = 0;
	u64 elapsed = -1, min_elapsed = -1;
	int i = -1;

	// Consider the minimal elapsed time reached after the requested number
	// of iterations hasn't provided a lesser value.
	while (++i < iterations && !quit) {
		clock_gettime(CLOCK_MONOTONIC, &t1);
		quit = func(data);
		clock_gettime(CLOCK_MONOTONIC, &t2);

		elapsed = (t2.tv_sec * 1000000000 + t2.tv_nsec) -
			(t1.tv_sec * 1000000000 + t1.tv_nsec);
		if (elapsed < min_elapsed) {
			min_elapsed = elapsed;
			i = -1;
		}
	}

	return quit ? 0 : elapsed;
}

bool
init_fence_sync_funcs(struct FenceSyncFuncs *funcs, EGLDisplay display)
{
	const char *ext = (const char*) eglQueryString(display,
							EGL_EXTENSIONS);

	if (ext &&
	    check_extension(ext, "EGL_KHR_fence_sync") &&
	    check_extension(ext, "EGL_ANDROID_native_fence_sync")) {
		// EGL_KHR_fence_sync
		funcs->create_sync =
			(void*) eglGetProcAddress("eglCreateSyncKHR");
		funcs->destroy_sync =
			(void*) eglGetProcAddress("eglDestroySyncKHR");
		assert(funcs->create_sync);
		assert(funcs->destroy_sync);

		// EGL_ANDROID_native_fence_sync
		funcs->dup_native_fence_fd =
			(void*) eglGetProcAddress("eglDupNativeFenceFDANDROID");
		assert(funcs->dup_native_fence_fd);

		return true;
	} else {
		fprintf(stderr, "Error: Native fence sync not supported.\n");
		return false;
	}
}

bool
init_disjoint_timer_query_funcs(struct DisjointTimerQueryFuncs *funcs)
{
	const char *ext = (const char*) glGetString(GL_EXTENSIONS);

	if (ext && check_extension(ext, "GL_EXT_disjoint_timer_query")) {
		PFNGLGETQUERYIVEXTPROC get_query_iv =
			(void*) eglGetProcAddress("glGetQueryivEXT");
		funcs->gen_queries =
			(void*) eglGetProcAddress("glGenQueriesEXT");
		funcs->delete_queries =
			(void*) eglGetProcAddress("glDeleteQueriesEXT");
		funcs->begin_query =
			(void*) eglGetProcAddress("glBeginQueryEXT");
		funcs->end_query = (void*) eglGetProcAddress("glEndQueryEXT");
		funcs->get_query_object_iv =
			(void*) eglGetProcAddress("glGetQueryObjectivEXT");
		funcs->get_query_object_ui64v =
			(void*) eglGetProcAddress("glGetQueryObjectui64vEXT");
		assert(get_query_iv);
		assert(funcs->gen_queries);
		assert(funcs->delete_queries);
		assert(funcs->begin_query);
		assert(funcs->end_query);
		assert(funcs->get_query_object_iv);

		int elapsed_bits;
		get_query_iv(GL_TIME_ELAPSED_EXT, GL_QUERY_COUNTER_BITS_EXT,
			     &elapsed_bits);
		assert(elapsed_bits != 0);

		return true;
	} else {
		fprintf(stderr, "Error: Disjoint timer query not supported.\n");
		return false;
	}
}

u64
elapsed_time_gpu(EGLDisplay display, int iterations, TaskFunc func, void *data)
{
	const int poll_timeout = 1000;  // 1 sec.
	const EGLint sync_attribs[] = { EGL_NONE };
	struct pollfd fds = { .events = POLLIN };
	u64 elapsed, min_elapsed = -1;
	int quit = 0;
	int i = -1;
	struct FenceSyncFuncs fence;
	struct DisjointTimerQueryFuncs timer;
	GLuint query;

	if (!init_fence_sync_funcs(&fence, display))
		return 0;
	if (!init_disjoint_timer_query_funcs(&timer))
		return 0;

	timer.gen_queries(1, &query);

	// Consider the minimal elapsed time reached after the requested number
	// of iterations hasn't provided a lesser value.
	while (++i < iterations && !quit) {
		EGLSyncKHR sync;
		int disjoint, available;

		// Issue commands.
		timer.begin_query(GL_TIME_ELAPSED_EXT, query);
		quit = func(data);
		timer.end_query(GL_TIME_ELAPSED_EXT);

		// Wait for completion.
		sync = fence.create_sync(display, EGL_SYNC_NATIVE_FENCE_ANDROID,
					 sync_attribs);
		assert(sync != EGL_NO_SYNC_KHR);
		glFlush();
		fds.fd = fence.dup_native_fence_fd(display, sync);
		assert(fds.fd != EGL_NO_NATIVE_FENCE_FD_ANDROID);
		poll(&fds, 1, poll_timeout);
		fence.destroy_sync(display, sync);
		close(fds.fd);

		// Get elapsed time, discard if disjoint.
		disjoint = 1;
		available = 0;
		timer.get_query_object_iv(query, GL_QUERY_RESULT_AVAILABLE,
					   &available);
		assert(available);
		glGetIntegerv(GL_GPU_DISJOINT_EXT, &disjoint);
		elapsed = -1;
		timer.get_query_object_ui64v(query, GL_QUERY_RESULT_EXT,
					      &elapsed);
		if (disjoint || elapsed == -1)
			continue;

		if (elapsed < min_elapsed) {
			min_elapsed = elapsed;
			i = -1;
		}
	}

	timer.delete_queries(1, &query);

	return quit ? 0 : elapsed;
}

static void
add_window_output(struct SurfaceWayland *surface, struct wl_output *wl_output)
{
	struct Output *output;
	struct Output *output_found = NULL;
	struct WindowOutput *window_output;

	wl_list_for_each(output, &surface->output_list, link) {
		if (output->wl_output == wl_output) {
			output_found = output;
			break;
		}
	}

	if (!output_found)
		return;

	window_output = malloc(sizeof *window_output);
	assert(window_output);

	window_output->output = output_found;

	wl_list_insert(surface->window_output_list.prev, &window_output->link);
	surface->update_geometry = true;
}

static void
destroy_window_output(struct SurfaceWayland *surface, struct wl_output *wl_output)
{
	struct WindowOutput *window_output;
	struct WindowOutput *window_output_found = NULL;

	wl_list_for_each(window_output, &surface->window_output_list, link) {
		if (window_output->output->wl_output == wl_output) {
			window_output_found = window_output;
			break;
		}
	}

	if (window_output_found) {
		wl_list_remove(&window_output_found->link);
		free(window_output_found);
		surface->update_geometry = true;
	}
}

static void
xdg_wm_base_ping(void *data, struct xdg_wm_base *shell, uint32_t serial)
{
	xdg_wm_base_pong(shell, serial);
}

static const struct xdg_wm_base_listener wm_base_listener = {
	xdg_wm_base_ping
};

static void
handle_display_geometry(void *data, struct wl_output *wl_output, int32_t x,
			int32_t y, int32_t physical_width,
			int32_t physical_height, int32_t subpixel,
			const char *make, const char *model, int32_t transform)
{
	struct Output *output = data;

	output->transform = transform;
	output->surface->update_geometry = true;
}

static void
handle_display_mode(void *data, struct wl_output *wl_output, uint32_t flags,
		    int32_t width, int32_t height, int32_t refresh)
{
}

static void
handle_display_done(void *data, struct wl_output *wl_output)
{
}

static void
handle_display_scale(void *data, struct wl_output *wl_output, int32_t scale)
{
	struct Output *output = data;

	output->scale = scale;
	output->surface->update_geometry = true;
}

static const struct wl_output_listener output_listener = {
	handle_display_geometry,
	handle_display_mode,
	handle_display_done,
	handle_display_scale
};

static void
display_add_output(struct SurfaceWayland *surface, uint32_t name)
{
	struct Output *output;

	output = calloc(1, sizeof *output);
	assert(output);

	output->surface = surface;
	output->scale = 1;
	output->wl_output = wl_registry_bind(surface->registry, name,
					     &wl_output_interface, 2);
	output->name = name;
	wl_list_insert(surface->output_list.prev, &output->link);

	wl_output_add_listener(output->wl_output, &output_listener, output);
}

static void
display_destroy_output(struct SurfaceWayland *surface, struct Output *output)
{
	destroy_window_output(surface, output->wl_output);
	wl_output_destroy(output->wl_output);
	wl_list_remove(&output->link);
	free(output);
}

static void
display_destroy_outputs(struct SurfaceWayland *surface)
{
	struct Output *tmp;
	struct Output *output;

	wl_list_for_each_safe(output, tmp, &surface->output_list, link)
		display_destroy_output(surface, output);
}

static void
handle_keyboard_keymap(void *data, struct wl_keyboard *keyboard,
		       uint32_t format, int fd, uint32_t size)
{
	close(fd);
}

static void
handle_keyboard_enter(void *data, struct wl_keyboard *keyboard, uint32_t serial,
		      struct wl_surface *surface, struct wl_array *keys)
{
}

static void
handle_keyboard_leave(void *data, struct wl_keyboard *keyboard, uint32_t serial,
		      struct wl_surface *surface)
{
}

static void
handle_keyboard_key(void *data, struct wl_keyboard *keyboard, uint32_t serial,
		    uint32_t time, uint32_t key, uint32_t state)
{
	struct SurfaceWayland *surface = data;

	if (!surface->wm_base || !state)
		return;

	if (surface->handlers && surface->handlers->keyboard_key)
		surface->handlers->keyboard_key(surface, time, key, state);
}

static void
handle_keyboard_modifiers(void *data, struct wl_keyboard *keyboard,
			  uint32_t serial, uint32_t mods_depressed,
			  uint32_t mods_latched, uint32_t mods_locked,
			  uint32_t group)
{
}

static const struct wl_keyboard_listener keyboard_listener = {
	handle_keyboard_keymap,
	handle_keyboard_enter,
	handle_keyboard_leave,
	handle_keyboard_key,
	handle_keyboard_modifiers
};

static void
handle_pointer_enter(void *data, struct wl_pointer *pointer, uint32_t serial,
		     struct wl_surface *wl_surface, wl_fixed_t px,
		     wl_fixed_t py)
{
	struct SurfaceWayland *surface = data;
	double x = wl_fixed_to_double(px) * surface->buffer_scale;
	double y = wl_fixed_to_double(py) * surface->buffer_scale;

	if (surface->handlers && surface->handlers->pointer_enter)
		surface->handlers->pointer_enter(surface, x, y);
}

static void
handle_pointer_leave(void *data, struct wl_pointer *pointer, uint32_t serial,
		     struct wl_surface *wl_surface)
{
	struct SurfaceWayland *surface = data;

	if (surface->handlers && surface->handlers->pointer_leave)
		surface->handlers->pointer_leave(surface);
}

static void
handle_pointer_motion(void *data, struct wl_pointer *pointer,
		      uint32_t time, wl_fixed_t px, wl_fixed_t py)
{
	struct SurfaceWayland *surface = data;
	double x = wl_fixed_to_double(px) * surface->buffer_scale;
	double y = wl_fixed_to_double(py) * surface->buffer_scale;

	if (surface->handlers && surface->handlers->pointer_motion)
		surface->handlers->pointer_motion(surface, time, x, y);
}

static void
handle_pointer_button(void *data, struct wl_pointer *wl_pointer,
		      uint32_t serial, uint32_t time, uint32_t button,
		      uint32_t state)
{
	struct SurfaceWayland *surface = data;

	if (surface->handlers && surface->handlers->pointer_button)
		surface->handlers->pointer_button(surface, time, button, state);
}

static void
handle_pointer_axis(void *data, struct wl_pointer *wl_pointer,
		    uint32_t time, uint32_t axis, wl_fixed_t value)
{
	struct SurfaceWayland *surface = data;

	if (surface->handlers && surface->handlers->pointer_axis)
		surface->handlers->pointer_axis(surface, time, axis,
						wl_fixed_to_int(value));
}

static const struct wl_pointer_listener pointer_listener = {
	handle_pointer_enter,
	handle_pointer_leave,
	handle_pointer_motion,
	handle_pointer_button,
	handle_pointer_axis
};

static void
handle_seat_capabilities(void *data, struct wl_seat *seat,
			 enum wl_seat_capability caps)
{
	struct SurfaceWayland *surface = data;

	if ((caps & WL_SEAT_CAPABILITY_KEYBOARD) && !surface->keyboard) {
		surface->keyboard = wl_seat_get_keyboard(seat);
		wl_keyboard_add_listener(surface->keyboard, &keyboard_listener,
					 surface);
	} else if (!(caps & WL_SEAT_CAPABILITY_KEYBOARD) && surface->keyboard) {
		wl_keyboard_destroy(surface->keyboard);
		surface->keyboard = NULL;
	}

	if ((caps & WL_SEAT_CAPABILITY_POINTER) && !surface->pointer) {
		surface->pointer = wl_seat_get_pointer(seat);
		wl_pointer_add_listener(surface->pointer, &pointer_listener,
					surface);
	} else if (!(caps & WL_SEAT_CAPABILITY_POINTER) && surface->pointer) {
		wl_pointer_destroy(surface->pointer);
		surface->pointer = NULL;
	}
}

static const struct wl_seat_listener seat_listener = {
	handle_seat_capabilities
};

static void
surface_enter(void *data, struct wl_surface *wl_surface,
	      struct wl_output *wl_output)
{
	add_window_output(data, wl_output);
}

static void
surface_leave(void *data, struct wl_surface *wl_surface,
	      struct wl_output *wl_output)
{
	destroy_window_output(data, wl_output);
}

static const struct wl_surface_listener surface_listener = {
	surface_enter,
	surface_leave
};

static void
handle_surface_configure(void *data, struct xdg_surface *xdg_surface,
			 uint32_t serial)
{
	struct SurfaceWayland *surface = data;

	xdg_surface_ack_configure(xdg_surface, serial);

	surface->wait_configure = false;
}

static const struct xdg_surface_listener xdg_surface_listener = {
	handle_surface_configure
};

static void
handle_toplevel_configure(void *data, struct xdg_toplevel *toplevel,
			  int32_t width, int32_t height,
			  struct wl_array *states)
{
	struct SurfaceWayland *surface = data;
	uint32_t *p;

	surface->fullscreen = false;
	surface->maximized = false;
	wl_array_for_each(p, states) {
		uint32_t state = *p;
		switch (state) {
		case XDG_TOPLEVEL_STATE_FULLSCREEN:
			surface->fullscreen = true;
			break;
		case XDG_TOPLEVEL_STATE_MAXIMIZED:
			surface->maximized = true;
			break;
		}
	}

	if (width > 0 && height > 0) {
		if (!surface->fullscreen && !surface->maximized) {
			surface->window_size.width = width;
			surface->window_size.height = height;
		}
		surface->logical_size.width = width;
		surface->logical_size.height = height;
	} else if (!surface->fullscreen && !surface->maximized) {
		surface->logical_size = surface->window_size;
	}

	surface->update_geometry = true;
}

static void
handle_toplevel_close(void *data, struct xdg_toplevel *xdg_toplevel)
{
	struct SurfaceWayland *surface = data;

	if (surface->handlers && surface->handlers->close)
		surface->handlers->close(surface);
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
	handle_toplevel_configure,
	handle_toplevel_close
};

static void
handle_registry_global(void *data, struct wl_registry *registry,
		       uint32_t name, const char *interface, uint32_t version)
{
	struct SurfaceWayland *surface = data;

	if (!strcmp(interface, wl_compositor_interface.name)) {
		surface->compositor = wl_registry_bind(
			registry, name, &wl_compositor_interface,
			MIN(version, 4));
	} else if (!strcmp(interface, xdg_wm_base_interface.name)) {
		surface->wm_base = wl_registry_bind(
			registry, name, &xdg_wm_base_interface, 1);
		xdg_wm_base_add_listener(
			surface->wm_base, &wm_base_listener, surface);
	} else if (!strcmp(interface, wl_seat_interface.name)) {
		surface->seat = wl_registry_bind(
			registry, name, &wl_seat_interface, 1);
		wl_seat_add_listener(surface->seat, &seat_listener, surface);
	} else if (!strcmp(interface, wl_output_interface.name) &&
		   version >= 2) {
		display_add_output(surface, name);
	}
}

static void
handle_registry_global_remove(void *data, struct wl_registry *registry,
			      uint32_t name)
{
	struct SurfaceWayland *surface = data;
	struct Output *output;

	wl_list_for_each(output, &surface->output_list, link) {
		if (output->name == name) {
			display_destroy_output(surface, output);
			break;
		}
	}
}

static const struct wl_registry_listener registry_listener = {
	handle_registry_global,
	handle_registry_global_remove
};

static int
get_buffer_scale(struct SurfaceWayland *surface)
{
	struct WindowOutput *window_output;
	int scale = 1;

	wl_list_for_each(window_output, &surface->window_output_list, link) {
		if (window_output->output->scale > scale)
			scale = window_output->output->scale;
	}

	return scale;
}

static enum wl_output_transform
get_buffer_transform(struct SurfaceWayland *surface)
{
	struct WindowOutput *window_output;
	enum wl_output_transform transform = WL_OUTPUT_TRANSFORM_NORMAL;

	wl_list_for_each(window_output, &surface->window_output_list, link) {
		transform = window_output->output->transform;
		break;
	}

	return transform;
}

static bool
maybe_update_geometry(struct SurfaceWayland *surface)
{
	enum wl_output_transform transform;
	struct Geometry buffer_size;
	int buffer_scale;

	if (!surface->update_geometry)
		return false;

	surface->update_geometry = false;

	transform = get_buffer_transform(surface);
	if (surface->buffer_transform != transform) {
		surface->buffer_transform = transform;
		wl_surface_set_buffer_transform(surface->wl_surface, transform);
	}

	switch (transform) {
	case WL_OUTPUT_TRANSFORM_NORMAL:
	case WL_OUTPUT_TRANSFORM_180:
	case WL_OUTPUT_TRANSFORM_FLIPPED:
	case WL_OUTPUT_TRANSFORM_FLIPPED_180:
		buffer_size.width = surface->logical_size.width;
		buffer_size.height = surface->logical_size.height;
		break;
	case WL_OUTPUT_TRANSFORM_90:
	case WL_OUTPUT_TRANSFORM_270:
	case WL_OUTPUT_TRANSFORM_FLIPPED_90:
	case WL_OUTPUT_TRANSFORM_FLIPPED_270:
		buffer_size.width = surface->logical_size.height;
		buffer_size.height = surface->logical_size.width;
		break;
	}

	buffer_scale = get_buffer_scale(surface);
	if (surface->buffer_scale != buffer_scale) {
		surface->buffer_scale = buffer_scale;
		wl_surface_set_buffer_scale(surface->wl_surface,
					    surface->buffer_scale);
	}
	buffer_size.width *= buffer_scale;
	buffer_size.height *= buffer_scale;

	if (surface->buffer_size.width != buffer_size.width ||
	    surface->buffer_size.height != buffer_size.height) {
		surface->buffer_size = buffer_size;
		return true;
	}

	return false;
}

static EGLSurface
create_egl_surface(EGLDisplay dpy, EGLConfig config, void *native,
		   const EGLint *attributes)
{
	static PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC
		create_platform_window = NULL;

	if (!create_platform_window)
		create_platform_window =
			(PFNEGLCREATEPLATFORMWINDOWSURFACEEXTPROC)
			get_egl_proc_address("eglCreatePlatformWindowSurfaceEXT");

	if (create_platform_window)
		return create_platform_window(dpy, config, native, attributes);

	return eglCreateWindowSurface(dpy, config, (EGLNativeWindowType)
				      native, attributes);
}

struct SurfaceWayland*
create_surface_wayland(int gl_major, int gl_minor, int width, int height,
		       const char *title, const char *id,
		       const struct SurfaceWaylandHandlers *handlers,
		       void *user_data)
{
	const EGLint context_attribs[] = {
		EGL_CONTEXT_MAJOR_VERSION, gl_major,
		EGL_CONTEXT_MINOR_VERSION, gl_minor,
		EGL_NONE
	};
	const EGLint config_attribs[] = {
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE,
		gl_major == 3 ? EGL_OPENGL_ES3_BIT : EGL_OPENGL_ES2_BIT,
		EGL_NONE
	};
	struct SurfaceWayland *surface;
	EGLint egl_major, egl_minor, nconfigs, nchoosen;
	EGLConfig *configs;

	surface = calloc(1, sizeof *surface);
	assert(surface);

	surface->buffer_size.width = width;
	surface->buffer_size.height = height;
	surface->window_size = surface->buffer_size;
	surface->buffer_scale = 1;
	surface->buffer_transform = WL_OUTPUT_TRANSFORM_NORMAL;
	surface->wait_configure = true;
	wl_list_init(&surface->output_list);
	wl_list_init(&surface->window_output_list);
	surface->handlers = handlers;
	surface->user_data = user_data;

	// Wayland.
	surface->display = wl_display_connect(NULL);
	if (!surface->display) {
		fprintf(stderr, "Error: Cannot connect to default Wayland "
			"display.");
		goto error_surface;
	}
	surface->registry = wl_display_get_registry(surface->display);
	wl_registry_add_listener(surface->registry, &registry_listener, surface);
	wl_display_roundtrip(surface->display);
	if (!surface->wm_base) {
		fprintf(stderr, "Error: Wayland xdg-shell support required.\n");
		goto error_wl_1;
	}
	surface->wl_surface = wl_compositor_create_surface(surface->compositor);
	wl_surface_add_listener(surface->wl_surface, &surface_listener,
				surface);
	surface->xdg_surface = xdg_wm_base_get_xdg_surface(surface->wm_base,
							   surface->wl_surface);
	xdg_surface_add_listener(surface->xdg_surface, &xdg_surface_listener,
				 surface);
	surface->xdg_toplevel = xdg_surface_get_toplevel(surface->xdg_surface);
	xdg_toplevel_add_listener(surface->xdg_toplevel, &xdg_toplevel_listener,
				  surface);
	xdg_toplevel_set_title(surface->xdg_toplevel, title);
	xdg_toplevel_set_app_id(surface->xdg_toplevel, id);
	wl_surface_commit(surface->wl_surface);
	while (wl_display_dispatch(surface->display) != -1 &&
	       surface->wait_configure);
	maybe_update_geometry(surface);

	// EGL.
	surface->egl.display = get_egl_display(EGL_PLATFORM_WAYLAND_KHR,
					       surface->display, NULL);
	if (!surface->egl.display) {
		fprintf(stderr, "Error: Cannot get EGL display.\n");
		goto error_wl_2;
	}
	if (!eglInitialize(surface->egl.display, &egl_major, &egl_minor)) {
		fprintf(stderr, "Error: Cannot initialise EGL.\n");
		goto error_wl_2;
	}
	if (!eglBindAPI(EGL_OPENGL_ES_API)) {
		fprintf(stderr, "Error: Cannot bind OpenGL ES API.\n");
		goto error_egl_1;
	}
	if (!eglGetConfigs(surface->egl.display, NULL, 0, &nconfigs) ||
	    nconfigs < 1) {
		fprintf(stderr, "Error: Cannot get EGL configs.\n");
		goto error_egl_1;
	}
	configs = calloc(nconfigs, sizeof *configs);
	assert(configs);
	if (!eglChooseConfig(surface->egl.display, config_attribs, configs,
			     nconfigs, &nchoosen) || nchoosen < 1) {
		fprintf(stderr, "Error: Cannot choose EGL config.\n");
		free(configs);
		goto error_egl_1;
	}
	surface->egl.config = configs[0];
	free(configs);

	// EGL Wayland.
	surface->native = wl_egl_window_create(
		surface->wl_surface, surface->buffer_size.width,
		surface->buffer_size.height);
	if (!surface->native) {
		fprintf(stderr, "Error: Cannot create native Wayland "
			"window.\n");
		goto error_egl_1;
	}
	surface->egl.surface = create_egl_surface(
		surface->egl.display, surface->egl.config, surface->native,
		NULL);
	if (surface->egl.surface == EGL_NO_SURFACE) {
		fprintf(stderr, "Error: Cannot create EGL surface.\n");
		goto error_egl_2;
	}

	// OpenGL ES.
	surface->egl.context = eglCreateContext(
		surface->egl.display, surface->egl.config, EGL_NO_CONTEXT,
		context_attribs);
	if (!surface->egl.context) {
		fprintf(stderr, "Error: Cannot create OpenGL ES %d.%d "
			"context.\n", gl_major, gl_minor);
		goto error_egl_3;
	}
	if (!eglMakeCurrent(surface->egl.display, surface->egl.surface,
			    surface->egl.surface, surface->egl.context)) {
		fprintf(stderr, "Error: Cannot make context current.\n");
		goto error_gl;
	}

	return surface;

 error_gl:
	eglDestroyContext(surface->egl.display, surface->egl.context);
 error_egl_3:
	eglDestroySurface(surface->egl.display, surface->egl.surface);
 error_egl_2:
	wl_egl_window_destroy(surface->native);
 error_egl_1:
	eglTerminate(surface->egl.display);
	eglReleaseThread();
 error_wl_2:
	if (surface->xdg_toplevel)
		xdg_toplevel_destroy(surface->xdg_toplevel);
	if (surface->xdg_surface)
		xdg_surface_destroy(surface->xdg_surface);
	wl_surface_destroy(surface->wl_surface);
	display_destroy_outputs(surface);
 error_wl_1:
	wl_registry_destroy(surface->registry);
	wl_display_flush(surface->display);
	wl_display_disconnect(surface->display);
 error_surface:
	free(surface);

	return NULL;
}

void
destroy_surface_wayland(struct SurfaceWayland *surface)
{
	eglMakeCurrent(surface->egl.display, EGL_NO_SURFACE, EGL_NO_SURFACE,
		       EGL_NO_CONTEXT);
	eglDestroyContext(surface->egl.display, surface->egl.context);
	eglDestroySurface(surface->display, surface->egl.surface);
	wl_egl_window_destroy(surface->native);
	eglTerminate(surface->egl.display);
	eglReleaseThread();
	if (surface->xdg_toplevel)
		xdg_toplevel_destroy(surface->xdg_toplevel);
	if (surface->xdg_surface)
		xdg_surface_destroy(surface->xdg_surface);
	wl_surface_destroy(surface->wl_surface);
	display_destroy_outputs(surface);
	wl_registry_destroy(surface->registry);
	wl_display_flush(surface->display);
	wl_display_disconnect(surface->display);
	free(surface);
}

int
dispatch_surface_wayland(struct SurfaceWayland *surface)
{
	if (wl_display_dispatch_pending(surface->display) == -1)
		return 1;

	if (maybe_update_geometry(surface)) {
		if (surface->native)
			wl_egl_window_resize(surface->native,
					     surface->buffer_size.width,
					     surface->buffer_size.height, 0, 0);
		if (surface->handlers && surface->handlers->resize)
			surface->handlers->resize(
				surface, (const struct Geometry)
				surface->buffer_size);
	}

	return 0;
}

struct Surface*
create_surface(int gl_major, int gl_minor)
{
	const EGLint context_attribs[] = {
		EGL_CONTEXT_MAJOR_VERSION, gl_major,
		EGL_CONTEXT_MINOR_VERSION, gl_minor,
		EGL_NONE
	};
	const EGLint config_attribs[] = {
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE,
		gl_major == 3 ? EGL_OPENGL_ES3_BIT : EGL_OPENGL_ES2_BIT,
		EGL_NONE
	};
	struct Surface *surface;
	EGLint egl_major, egl_minor, nconfigs, nchoosen;
	EGLConfig *configs;

	surface = calloc(1, sizeof *surface);
	assert(surface);

	// EGL.
	surface->display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	if (!surface->display) {
		fprintf(stderr, "Error: Cannot get EGL display.\n");
		goto error_surface;
	}
	if (!eglInitialize(surface->display, &egl_major, &egl_minor)) {
		fprintf(stderr, "Error: Cannot initialise EGL.\n");
		goto error_egl;
	}
	if (!eglBindAPI(EGL_OPENGL_ES_API)) {
		fprintf(stderr, "Error: Cannot bind OpenGL ES API.\n");
		goto error_egl;
	}
	if (!eglGetConfigs(surface->display, NULL, 0, &nconfigs) ||
	    nconfigs < 1) {
		fprintf(stderr, "Error: Cannot get EGL configs.\n");
		goto error_egl;
	}
	configs = calloc(nconfigs, sizeof *configs);
	assert(configs);
	if (!eglChooseConfig(surface->display, config_attribs, configs,
			     nconfigs, &nchoosen) || nchoosen < 1) {
		fprintf(stderr, "Error: Cannot choose EGL config.\n");
		free(configs);
		goto error_egl;
	}
	surface->config = configs[0];
	free(configs);

	// OpenGL ES.
	surface->context = eglCreateContext(
		surface->display, surface->config, EGL_NO_CONTEXT,
		context_attribs);
	if (!surface->context) {
		fprintf(stderr, "Error: Cannot create OpenGL ES %d.%d "
			"context.\n", gl_major, gl_minor);
		goto error_egl;
	}
	if (!eglMakeCurrent(surface->display, EGL_NO_SURFACE, EGL_NO_SURFACE,
			    surface->context)) {
		fprintf(stderr, "Error: Cannot make context current.\n");
		goto error_gl;
	}

	return surface;

 error_gl:
	eglDestroyContext(surface->display, surface->context);
 error_egl:
	eglTerminate(surface->display);
	eglReleaseThread();
 error_surface:
	free(surface);

	return NULL;
}

void
destroy_surface(struct Surface *surface)
{
	eglMakeCurrent(surface->display, EGL_NO_SURFACE, EGL_NO_SURFACE,
		       EGL_NO_CONTEXT);
	eglDestroyContext(surface->display, surface->context);
	eglTerminate(surface->display);
	eglReleaseThread();
	free(surface);
}
