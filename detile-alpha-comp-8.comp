#version 310 es

// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include "common.glsl"

// ARM GPU best practices developer guide recommends no more than 64 threads per
// workgroup and multiples of 4 workgroup sizes.

// 1 2x32 workgroup loads 1 entire Y tile.
// 1 2x32 workgroup loads 2 entire UV tiles.
#define THREAD_X 2
#define THREAD_Y 32

layout(local_size_x = THREAD_X, local_size_y = THREAD_Y) in;

layout(location = 0) uniform uint tiles_per_stride;
layout(location = 1) uniform uint no_csc;

layout(rgba16ui, binding = 0) readonly  uniform highp uimage2D y_tiled;
layout(rgba16ui, binding = 1) readonly  uniform highp uimage2D uv_tiled;
layout(rgba16ui, binding = 3) readonly  uniform highp uimage2D a_tiled;
layout(rgba32ui, binding = 2) writeonly uniform highp uimage2D rgba_linear;

// Convert from linear coordinates to tiled coordinates.
// Y coords are returned in x and z, UV coords are returned in y and w.
ivec4
tiled_from_linear(ivec2 linear)
{
	// Constants:
	//   kTileW    = MM21_TILE_WIDTH / 8               =  2
	//   kYTileH   = MM21_Y_TILE_HEIGHT                = 32
	//   kUVTileH  = MM21_UV_TILE_HEIGHT               = 16
	//   kYTileSz  = kTileW * kYTileH                  = 64
	//   kUVTileSz = (kTileW / 2) * (kUVTileH / 2) * 2 = 32

	// 2D tile coord addressed by 'linear' in the linear buffer (1st tile is
	// (0,0), 2nd tile is (1,0), etc):
	//   tlc = linear.xy / (kTileW, kYTileH)
	uvec2 tlc = uvec2(linear) >> uvec2(1u, 5u);

	// 2D texel coord addressed by 'linear' in the tile:
	//   txc = linear.xy % (kTileW, kYTileH)
	uvec2 txc = uvec2(linear) & uvec2(1u, 31u);

	// Tile offset.
	uint tlo = tlc.y * tiles_per_stride + tlc.x;

	// Texel offset in Y and UV buffers:
	//   txo.x = (txc.y * kTileW) + txc.x
	//   txo.y = (((linear.y / 2) % kUVTileH)      * kTileW) + txc.x
	//         = (((linear.y      % kYTileH ) / 2) * kTileW) + txc.x
	//         = ((txc.y                      / 2) * kTileW) + txc.x
	uvec2 txo = uvec2(txc.y << 1u, txc.y & ~1u) | txc.xx;

	// Global offset in Y and UV buffers:
	//   off.x = (tlo * kYTileSz) + txo.x
	//   off.y = (tlo * 2 * kUVTileSz) + txo.y
	uvec2 off = (uvec2(tlo) << uvec2(6u, 5u)) | txo;

	// XXX Hard-coded for images of width of 1920 for now!
	//
	// Convert to 2D coord in the tiled buffer:
	//   tiled.xy = off % (1920 / 8, 1920 / 8)
	//   tiled.zw = off / (1920 / 8, 1920 / 8)
	//
	// Move div/mod computation to floating-point pipe using reciprocal mul
	// as it's very slow otherwise on the integer pipe.
	vec2 q = vec2(off) * vec2(0.00416666666666666667);
	return ivec4(uvec2(fract(q) * vec2(240.0)), uvec2(q));
}

void
main()
{
	ivec2 linear = ivec2(gl_GlobalInvocationID.xy);
	ivec4 tiled = tiled_from_linear(linear);

	uvec4 y = imageLoad(y_tiled, tiled.xz);
	uvec4 uv = imageLoad(uv_tiled, tiled.yw);
	uvec4 a = imageLoad(a_tiled, tiled.xz);

	uvec4 hi_y = y >> 8u, lo_y = y & 255u;
	uvec4 hi_uv = uv >> 8u, lo_uv = uv & 255u;
	uvec4 hi_a = a >> 8u, lo_a = a & 255u;

	if (no_csc == uint(0)) {
		uvec4 unpack_y = uvec4(lo_y.x, hi_y.x, lo_y.y, hi_y.y);
		uvec4 unpack_a = uvec4(lo_a.x, hi_a.x, lo_a.y, hi_a.y);
		uvec4 rgba = rgba_from_yuva_packed(unpack_y, lo_uv.xxyy, hi_uv.xxyy, unpack_a);
		imageStore(rgba_linear, ivec2(linear.x << 1, linear.y), rgba);

		unpack_y = uvec4(lo_y.z, hi_y.z, lo_y.w, hi_y.w);
		unpack_a = uvec4(lo_a.z, hi_a.z, lo_a.w, hi_a.w);
		rgba = rgba_from_yuva_packed(unpack_y, lo_uv.zzww, hi_uv.zzww, unpack_a);
		imageStore(rgba_linear, ivec2((linear.x << 1) | 1, linear.y), rgba);
	} else {
		uvec4 unpack_y = uvec4(lo_y.x, hi_y.x, lo_y.y, hi_y.y);
		uvec4 unpack_a = uvec4(lo_a.x, hi_a.x, lo_a.y, hi_a.y);
		uvec4 rgba = yuva_from_yuva_packed(unpack_y, lo_uv.xxyy, hi_uv.xxyy, unpack_a);
		imageStore(rgba_linear, ivec2(linear.x << 1, linear.y), rgba);

		unpack_y = uvec4(lo_y.z, hi_y.z, lo_y.w, hi_y.w);
		unpack_a = uvec4(lo_a.z, hi_a.z, lo_a.w, hi_a.w);
		rgba = yuva_from_yuva_packed(unpack_y, lo_uv.zzww, hi_uv.zzww, unpack_a);
		imageStore(rgba_linear, ivec2((linear.x << 1) | 1, linear.y), rgba);
	}
}
