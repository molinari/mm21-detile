// SPDX-FileCopyrightText:  © 2024 Collabora Ltd
// SPDX-License-Identifier: MIT

#include "benchmark.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <linux/input.h>

enum TextureId {
	TEXTURE_Y = 0,
	TEXTURE_UV,
	TEXTURE_ALPHA,
	TEXTURE_RGBA,
	TEXTURE_COUNT
};

enum ProgramId {
	PROGRAM_DETILE = 0,
	PROGRAM_VIEW,
	PROGRAM_COUNT
};

struct FragData {
	struct SurfaceWayland *surface;
	unsigned int framebuffer;
	unsigned int programs[PROGRAM_COUNT];
	unsigned int textures[TEXTURE_COUNT];
	const struct CommandLine *cli;
	const struct Image *image;
	bool quit;
};

static void
handle_keyboard_key(struct SurfaceWayland *surface, uint32_t time, uint32_t key,
		    uint32_t state)
{
	struct FragData *data = surface->user_data;

	switch (key) {
	case KEY_Q:
	case KEY_ESC:
		data->quit = true;
		break;
	default:
		break;
	}
}

static void
handle_close(struct SurfaceWayland *surface)
{
	struct FragData *data = surface->user_data;

	data->quit = true;
}

static const struct SurfaceWaylandHandlers wayland_handlers = {
	.keyboard_key = handle_keyboard_key,
	.pointer_enter = NULL,
	.pointer_leave = NULL,
	.pointer_motion = NULL,
	.pointer_button = NULL,
	.pointer_axis = NULL,
	.resize = NULL,
	.close = handle_close
};

static struct VariantData*
create_frag(const struct CommandLine *cli, const struct Image *image,
	    const struct Image *alpha)
{
	int y_tex_width, y_tex_height, uv_tex_width, uv_tex_height;
	int max_texture_size;
	unsigned int program;
	struct FragData *frag;

	frag = calloc(1, sizeof *frag);
	assert(frag);
	frag->cli = cli;
	frag->image = image;

	if (cli->view)
		frag->surface = create_surface_wayland(
			3, 1, image->width, image->height, WINDOW_TITLE,
			WINDOW_ID, &wayland_handlers, frag);
	else
		frag->surface = (struct SurfaceWayland*) create_surface(3, 1);
	if (!frag->surface)
		goto error;

	y_tex_width = image->stride;
	y_tex_height = image->h_stride;
	uv_tex_width = image->stride / 2;
	uv_tex_height = image->h_stride / 2;

	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_texture_size);
	if (y_tex_width > max_texture_size || y_tex_height > max_texture_size) {
		fprintf(stderr, "Error: Y texture size %dx%d higher than "
			"implementation limit %d.\n", y_tex_width, y_tex_height,
			max_texture_size);
		goto error_surface;
	}

	frag->programs[PROGRAM_DETILE] = compile_fragment_program(
		TEXTURE_VS_FILENAME, alpha ? DETILE_ALPHA_FRAG_FS_FILENAME :
		DETILE_FRAG_FS_FILENAME);
	if (!frag->programs[PROGRAM_DETILE])
		goto error_surface;

	glGenTextures(TEXTURE_COUNT, frag->textures);

	glGenFramebuffers(1, &frag->framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frag->framebuffer);
	glBindTexture(GL_TEXTURE_2D, frag->textures[TEXTURE_RGBA]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, image->width, image->height);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			       GL_TEXTURE_2D, frag->textures[TEXTURE_RGBA], 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, frag->textures[TEXTURE_Y]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_R8, y_tex_width, y_tex_height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, y_tex_width, y_tex_height,
			GL_RED, GL_UNSIGNED_BYTE, image->y);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, frag->textures[TEXTURE_UV]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RG8, uv_tex_width, uv_tex_height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, uv_tex_width, uv_tex_height,
			GL_RG, GL_UNSIGNED_BYTE, image->uv);

	if (alpha) {
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, frag->textures[TEXTURE_ALPHA]);
		glTexStorage2D(GL_TEXTURE_2D, 1, GL_R8, y_tex_width,
			       y_tex_height);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, y_tex_width,
				y_tex_height, GL_RED, GL_UNSIGNED_BYTE,
				alpha->y);
	}

	program = frag->programs[PROGRAM_DETILE];
	glUseProgram(program);
	glUniform1ui(glGetUniformLocation(program, "tiles_per_stride"),
		     image->stride / MM21_TILE_WIDTH);
	glUniform1ui(glGetUniformLocation(program, "no_csc"), cli->no_csc);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glDisable(GL_BLEND);

	if (cli->view) {
		frag->programs[PROGRAM_VIEW] = compile_fragment_program(
			TEXTURE_VS_FILENAME, TEXTURE_FS_FILENAME);
		if (!frag->programs[PROGRAM_VIEW])
			goto error_surface;

		if (cli->fullscreen)
			xdg_toplevel_set_fullscreen(frag->surface->xdg_toplevel,
						    NULL);

		eglSwapInterval(frag->surface->egl.display, 0);
	}

	return (struct VariantData*) frag;

 error_surface:
	if (cli->view)
		destroy_surface_wayland(frag->surface);
	else
		destroy_surface((struct Surface*) frag->surface);
 error:
	free(frag);

	return NULL;
}

static void
destroy_frag(struct VariantData *data)
{
	struct FragData *frag = (struct FragData*) data;

	glDeleteFramebuffers(1, &frag->framebuffer);
	glDeleteTextures(TEXTURE_COUNT, frag->textures);
	glDeleteProgram(frag->programs[PROGRAM_DETILE]);
	if (frag->cli->view) {
		glDeleteProgram(frag->programs[PROGRAM_VIEW]);
		destroy_surface_wayland(frag->surface);
	} else {
		destroy_surface((struct Surface*) frag->surface);
	}
	free(frag);
}

static int
run_frag(struct VariantData *data)
{
	struct FragData *frag = (struct FragData*) data;

	static const struct { float x, y; } detile_positions[4] = {
		{ -1.0f, -1.0f },
		{  1.0f, -1.0f },
		{ -1.0f,  1.0f },
		{  1.0f,  1.0f }
	};
	const struct { float s, t; } tex_coords[4] = {
		{ 0.0f,               0.0f },
		{ frag->image->width, 0.0f },
		{ 0.0f,               frag->image->height },
		{ frag->image->width, frag->image->height }
	};

	// Detile.
	glBindFramebuffer(GL_FRAMEBUFFER, frag->framebuffer);
	glViewport(0, 0, frag->image->width, frag->image->height);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, frag->textures[TEXTURE_Y]);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, frag->textures[TEXTURE_UV]);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, frag->textures[TEXTURE_ALPHA]);
	glUseProgram(frag->programs[PROGRAM_DETILE]);
	glVertexAttribPointer(0, 2, GL_FLOAT, 0, 0, detile_positions);
	glVertexAttribPointer(1, 2, GL_FLOAT, 0, 0, tex_coords);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	if (frag->cli->view) {
		struct SurfaceWayland *surface = frag->surface;
		float a = surface->buffer_size.width * frag->image->height;
		float b = surface->buffer_size.height * frag->image->width;
		float ratio_x = MIN(1.0f, b / a);
		float ratio_y = MIN(1.0f, a / b);
		struct { float x, y; } view_positions[4] = {
			{ -ratio_x,  ratio_y },
			{  ratio_x,  ratio_y },
			{ -ratio_x, -ratio_y },
			{  ratio_x, -ratio_y }
		};
		struct wl_region *region;

		// Draw.
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, surface->buffer_size.width,
			   surface->buffer_size.height);
		glClear(GL_COLOR_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, frag->textures[TEXTURE_RGBA]);
		glUseProgram(frag->programs[PROGRAM_VIEW]);
		glVertexAttribPointer(0, 2, GL_FLOAT, 0, 0, view_positions);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		// Present.
		region = wl_compositor_create_region(surface->compositor);
		wl_region_add(region, 0, 0, INT32_MAX, INT32_MAX);
		wl_surface_set_opaque_region(surface->wl_surface, region);
		wl_region_destroy(region);
		eglSwapBuffers(surface->egl.display, surface->egl.surface);

		return dispatch_surface_wayland(frag->surface) || frag->quit ||
			sigint;
	} else {
		// Flush and sync with GPU to get decent timings.
		if (frag->cli->timing == TIMING_CPU)
			glFinish();

		return sigint;
	}
}

static u8*
export_frag(struct VariantData *data)
{
	struct FragData *frag = (struct FragData*) data;
	u8 *pixels;

	pixels = malloc(frag->image->stride * frag->image->height * 4);
	assert(pixels);

	glReadPixels(0, 0, frag->image->stride, frag->image->height, GL_RGBA,
		     GL_UNSIGNED_BYTE, pixels);

	return pixels;
}

const struct VariantFuncs frag_funcs = {
	.create = create_frag,
	.destroy = destroy_frag,
	.run = run_frag,
	.export = export_frag
};
